package view;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import physics.Circle;
import Model.GameLoop;
import Model.Model_I_View;
import Model_Gizmos.Absorber;
import Model_Gizmos.Ball;
import Model_Gizmos.Gizmo_I;
import flipper.AbstractFlipper;

/**
 * @author Adam Campbell based on Murray's MVC Code
 * 
 */
public class Board extends JPanel implements Observer {

	private static final long serialVersionUID = 1L;

	/** Height and width of the Board */
	private int width;
	private int height;
	/** GameLoop to get L */
	private GameLoop gm;
	/** Boolean for line drawing */
	private boolean panelTF;

	/**
	 * Default constructor.
	 * @param w
	 * 		The width of the Board
	 * @param h
	 * 		The height of the Board
	 * @param model
	 * 		The model we're working with.
	 */
	public Board(int w, int h, Model_I_View model) {
		// Observe changes in Model
		gm = (GameLoop) model;
		gm.addObserver(this);

		// Set values
		width = w;
		height = h;
		this.setBorder(BorderFactory.createLineBorder(Color.black));
		this.setDoubleBuffered(true);
		this.setBackground(new Color(model.getBackgroundColour()));
	}

	/**
	 * Sets the correct size of the board.
	 */
	public Dimension getPreferredSize() 
	{ 
		return new Dimension(width, height);
	}

	/**
	 * @param panelTF
	 */
	public void setPanelTF(boolean panelTF) 
	{
		this.panelTF = panelTF;
	}

	/**
	 * @return panelTF
	 */
	public boolean getPanelTF()
	{
		return panelTF;
	}

	@Override
	public void paintComponent(Graphics g) 
	{
		super.paintComponent(g);

		// Set the new background colour
		this.setBackground(new Color(gm.getBackgroundColour()));

		Graphics2D g2 = (Graphics2D) g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		// Draw lines?
		if(panelTF == true)
		{
			Color lineColour = new Color(gm.getBackgroundColour());	
			int red = 255 - lineColour.getRed();
			int green = 255 - lineColour.getGreen();
			int blue = 255 - lineColour.getBlue();
			if((red >= 120 && red <= 130) && (green >= 120 && green <= 130) && (blue >= 120 && blue <= 130)){
				g2.setColor(Color.BLACK);
			}else{
				g2.setColor(new Color(red,green,blue));
			}
			for (int i = 0; i < 20; i++)
			{
				g.drawLine(0, translateFromL(i) , translateFromL(20), translateFromL(i) );
			}
			for (int j = 0; j < 20; j++)
			{
				g.drawLine(translateFromL(j) , 0, translateFromL(j), translateFromL(20));
			}
		}

		// Onto drawing Gizmos
		for (Gizmo_I giz : gm.getGizmos()) {

			// Set the colour
			g2.setColor(new Color(giz.getColour()));

			// Discern what type it is
			if (giz.getType().equals("Circle")) {

				g2.fillOval(translateFromL(giz.getX()), translateFromL(giz.getY()), GameLoop.getL(), GameLoop.getL());

			} else if (giz.getType().equals("Triangle")) {

				Set<Circle> points = giz.getCircles();

				int[] xPoints = new int[3];
				int[] yPoints = new int[3];
				int nPoints = 0;

				for (Circle c : points) {
					xPoints[nPoints] =  (int)(c.getCenter().x() * GameLoop.getL());
					yPoints[nPoints] =  (int)(c.getCenter().y() * GameLoop.getL());
					nPoints++;
				}

				g2.fillPolygon(xPoints, yPoints, nPoints);

			} else if(giz.getType().equals("LeftFlipper") || giz.getType().equals("RightFlipper")){

				g2.setStroke(new BasicStroke(GameLoop.getL()/2, BasicStroke.CAP_ROUND, 0));
				double[] lCoordinates = ((AbstractFlipper) giz).getFlipper();

				g2.drawLine((int)Math.round(lCoordinates[0]*GameLoop.getL()), 
						(int)Math.round(lCoordinates[1]*GameLoop.getL()), 
						(int)Math.round(lCoordinates[2]*GameLoop.getL()), 
						(int)Math.round(lCoordinates[3]*GameLoop.getL()));

			} else if(giz.getType().equals("Absorber")) {
				g2.fillRect(translateFromL(giz.getX()), translateFromL(giz.getY()), ((Absorber)giz).getWidth() * GameLoop.getL(), ((Absorber)giz).getHeight() * GameLoop.getL());
			} else if(giz.getType().equals("Ball")) {
				g2.setColor(new Color(giz.getColour()));

				int x = (int) (((Ball) giz).getExactX() * GameLoop.getL());
				int y = (int) (((Ball) giz).getExactY() * GameLoop.getL());

				g2.fillOval(x, y, GameLoop.getL()/2, GameLoop.getL()/2);
			} else {
				g2.fillRect(translateFromL(giz.getX()), translateFromL(giz.getY()), GameLoop.getL(), GameLoop.getL());
			}

		}

	}

	/**
	 * Translates a number from L to px
	 * @param n The L value to be converted to pixels.
	 * @return The pixel value corresponding to the L value passed in.
	 */
	private int translateFromL(int n) {
		return n * GameLoop.getL();
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		repaint();
	}

}
