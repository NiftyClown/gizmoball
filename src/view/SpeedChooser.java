package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;

import Model.Model_I_Controller;
import Model.Model_I_View;
import controller.SpeedController;

public class SpeedChooser extends JFrame
{
	private static final long serialVersionUID = -2244883021775729154L;
	/** Limits */
	static final int S_MIN = -30;
    static final int S_MAX = 30;
    static final int S_INIT = 0;
	/** Fonts of the class */
	private Font gf, mediumgf, smallgf;
	
	public SpeedChooser(Model_I_View m, int x, int y) {
		this.setSize(new Dimension(620,250));
		this.setLocation(640, 0);
		this.setTitle("Set Initial Speed");

		gf = new Font("Arial", Font.BOLD, 30);
		mediumgf = new Font("Arial", Font.BOLD, 16);
		smallgf = new Font("Arial", Font.BOLD, 12);
		 
		// Set a background for JFrame
        setContentPane(new JLabel(new ImageIcon("logos/bus.png")));
        
        // Set some layout, say FlowLayout
        setLayout(new BorderLayout());

        //Create the first slider
        JSlider speedX = new JSlider(JSlider.HORIZONTAL,S_MIN, S_MAX, S_INIT);
        setSliderProperties(speedX, 10, 1);
        speedX.setForeground(Color.RED);
        
        //create the second slider
        JSlider speedY = new JSlider(JSlider.HORIZONTAL,S_MIN, S_MAX, S_INIT);
        setSliderProperties(speedY, 10, 1);
        speedY.setForeground(Color.GREEN);
       
        //Add button for user finishing
        JButton done = new JButton("Done");
        done.setFont(smallgf);
        done.setToolTipText("Click here when you are done choosing a colour");
        done.addActionListener(new SpeedController((Model_I_Controller) m, x, y, speedX, speedY, this));
        
        //create labels for sliders
        JLabel xf = new JLabel("Change X Speed", JLabel.CENTER);
		xf.setAlignmentX(Component.CENTER_ALIGNMENT);
		xf.setFont(mediumgf);
		xf.setForeground(Color.RED);
		JLabel yf = new JLabel("Change Y Speed", JLabel.CENTER);
		yf.setFont(mediumgf);
		yf.setForeground(Color.GREEN);
		
		//create panels 
		JPanel top = new JPanel();
		top.setPreferredSize(new Dimension(this.getWidth(), 100));
		top.setOpaque(false);
		top.add(xf, BorderLayout.NORTH);
		top.add(speedX, BorderLayout.SOUTH);
		JPanel bottom = new JPanel();
		bottom.setPreferredSize(new Dimension(this.getWidth(), 100));
		bottom.setOpaque(false);
		bottom.add(yf, BorderLayout.NORTH);
		bottom.add(speedY, BorderLayout.SOUTH);
		
        //add to screen
        add(top, BorderLayout.NORTH);
        add(bottom, BorderLayout.CENTER);
        add(done, BorderLayout.SOUTH);
	}
	
	/**
	 * @param js
	 * @param x
	 * @param y
	 * 		Helper method to neaten creation of sliders
	 */
	public void setSliderProperties(JSlider js, int x, int y)
	{
		js.setPaintTicks(true);
		js.setPaintLabels(true);
		js.setFont(gf);
	    js.setMajorTickSpacing(x);
        js.setMinorTickSpacing(y);
        js.setOpaque(false);
        js.setPreferredSize(new Dimension(600,100));
	}
}