package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;

import Model.Model_I_Controller;
import Model.Model_I_View;
import controller.DoneListener;
import controller.SetFrictionController;

public class FrictionSlider extends JFrame 
{
	private static final long serialVersionUID = 8006070997735957640L;
	static final int F_MIN = 0;
	static final int F_MAX = 30;
	static final int F_INIT = 15;
	/** Font of the class */
	private Font gf, mediumgf, smallgf;
	/** The model we're working with */
	private Model_I_View gm;
	private DoneListener fl;

	public FrictionSlider(Model_I_View m)
	{
		this.setSize(new Dimension(620,250));
		this.setLocation(640, 0);
		this.setTitle("Set Friction");
		this.gm = m;

		gf = new Font("Arial", Font.BOLD, 30);
		mediumgf = new Font("Arial", Font.BOLD, 16);
		smallgf = new Font("Arial", Font.BOLD, 12);

		//Add listener for finishing
		fl = new DoneListener(this);

		// Set a background for JFrame
		setContentPane(new JLabel(new ImageIcon("logos/friction.png")));

		// Set some layout, say FlowLayout
		setLayout(new BorderLayout());

		//Create the slider.
		JSlider friction1 = new JSlider(JSlider.HORIZONTAL,F_MIN, F_MAX, F_INIT);
		JSlider friction2 = new JSlider(JSlider.HORIZONTAL,F_MIN, F_MAX, F_INIT);

		friction1.setPreferredSize(new Dimension(600,100));
		setSliderProperties(friction1, 10, 1);
		friction1.setForeground(Color.BLUE);
		friction1.addChangeListener(new SetFrictionController((Model_I_Controller) gm, friction1, friction2));

		friction2.setPreferredSize(new Dimension(600,100));
		setSliderProperties(friction2, 10, 1);
		friction2.setForeground(Color.BLUE);
		friction1.addChangeListener(new SetFrictionController((Model_I_Controller) gm, friction1, friction2));

		//Add button for user finishing
		JButton done = new JButton("Done");
		done.setFont(smallgf);
		done.setToolTipText("Click here when you are done choosing a colour");
		done.addActionListener(fl);

		//create labels for sliders
		JLabel xf = new JLabel("Change X Friction", JLabel.CENTER);
		xf.setAlignmentX(Component.CENTER_ALIGNMENT);
		xf.setFont(mediumgf);
		xf.setForeground(Color.RED);
		JLabel yf = new JLabel("Change Y Friction", JLabel.CENTER);
		yf.setFont(mediumgf);
		yf.setForeground(Color.RED);
		
		//create panels 
		JPanel top = new JPanel();
		top.setPreferredSize(new Dimension(this.getWidth(), 100));
		top.setOpaque(false);
		top.add(xf, BorderLayout.NORTH);
		top.add(friction1, BorderLayout.SOUTH);
		JPanel bottom = new JPanel();
		bottom.setPreferredSize(new Dimension(this.getWidth(), 100));
		bottom.setOpaque(false);
		bottom.add(yf, BorderLayout.NORTH);
		bottom.add(friction2, BorderLayout.SOUTH);
		
		//add to frame
		add(top, BorderLayout.NORTH);
		add(bottom, BorderLayout.CENTER);
		add(done, BorderLayout.SOUTH);
	}

	/**
	 * @param js
	 * @param x
	 * @param y
	 * 		Helper method to neaten creation of sliders
	 */
	public void setSliderProperties(JSlider js, int x, int y)
	{
		js.setPaintTicks(true);
		js.setPaintLabels(true);
		js.setFont(gf);
		js.setMajorTickSpacing(x);
		js.setMinorTickSpacing(y);
		js.setOpaque(false);
		js.setPreferredSize(new Dimension(600,100));
	}
}