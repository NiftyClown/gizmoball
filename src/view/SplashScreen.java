package view;

import java.awt.Color;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextPane;
import javax.swing.JWindow;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import controller.Task;

public class SplashScreen extends JWindow
{
	private static final long serialVersionUID = -3516438835488132029L;
	private int duration;
	private Font gf;
	private JProgressBar progressBar;
	private Task task;
	private String COPYRIGHT = "\u00a9";

	public SplashScreen(int d) 
	{
		duration = d;
		gf = new Font("Arial", Font.BOLD, 12);
	}

	public void showSplash() 
	{
		JPanel content = (JPanel)getContentPane();
		content.setBackground(Color.white);
		content.setLayout(new BoxLayout(content, BoxLayout.PAGE_AXIS));

		this.setSize(500,235);
		this.setLocationRelativeTo(null);

		// Build the splash screen
		JLabel label = new JLabel(new ImageIcon("logos/logo2.png"), JLabel.CENTER);
		label.setBackground(Color.BLACK);
		
		JTextPane about = new JTextPane();
		about.setText("CS308 GizmoBall Project for Group 12\n\nAdam Campbell, Barry McAuley, "
				+ "Nathan-Daniel Millar,\nMark Lee, Alexander Mckenna\n"
				+ COPYRIGHT + " 2014 Group W12");
		about.setFont(gf);
		
		StyledDocument doc = about.getStyledDocument();
		SimpleAttributeSet center = new SimpleAttributeSet();
		StyleConstants.setAlignment(center, StyleConstants.ALIGN_CENTER);
		doc.setParagraphAttributes(0, doc.getLength(), center, false);

		progressBar = new JProgressBar(0, 100);
		progressBar.setValue(0);
		progressBar.setStringPainted(true);
		progressBar.setFont(gf);

		content.add(label);
		content.add(progressBar);
		content.add(about);
		content.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 10));

		// Display it
		setVisible(true);

		// Wait a little while
		try
		{ 
			task = new Task(this);
			task.start();
			Thread.sleep(duration);
		} 
		catch (Exception e) 
		{
		}

		setVisible(false);
	}

	public void setProgress(int progress)
	{
		progressBar.setValue(progress); 
		progressBar.revalidate();
	}
}