package view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.lang.reflect.Array;
import java.lang.reflect.Field;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.colorchooser.AbstractColorChooserPanel;

import Model.Model_I_Controller;
import Model.Model_I_View;
import controller.DoneListener;
import controller.SetBackgroundColourController;
import controller.SetGizmoColourController;

public class ColourChooser extends JFrame
{
	private static final long serialVersionUID = -7020332212974548284L;
	/** The colour chooser. */
	private JColorChooser chooser;
	/** Another listener. */
	private DoneListener ccl;

	/** Font of the class.*/
	private Font gf;

	/**
	 * Default constructor
	 */
	public ColourChooser(Model_I_View m, int gridX, int gridY)
	{
		this.setSize(new Dimension(620,300));
		this.setLocation(640, 0);
		this.setTitle("Colour Chooser");
		gf = new Font("Arial", Font.BOLD, 12);

		//Set up color chooser for setting colour required
		chooser = new JColorChooser();
		chooser.setFont(gf);
		modifyPanels();
		chooser.getSelectionModel().addChangeListener(new SetGizmoColourController(chooser, (Model_I_Controller) m, gridX, gridY));


		add(chooser, BorderLayout.CENTER);

		ccl = new DoneListener(this);

		JButton done = new JButton("Done");
		done.setFont(gf);
		done.setToolTipText("Click here when you are done choosing a colour");
		done.addActionListener(ccl);

		add(done, BorderLayout.PAGE_END);
		this.setVisible(true);

	}

	/**
	 * Constructor for changing the background colour
	 * 
	 * @param m
	 * 		The model.
	 */
	public ColourChooser(Model_I_View m)
	{
		this.setSize(new Dimension(620,300));
		this.setLocation(640, 0);
		this.setTitle("Colour Chooser");

		gf = new Font("Arial", Font.BOLD, 12);

		//Set up color chooser for setting colour required
		chooser = new JColorChooser();
		chooser.setFont(gf);
		modifyPanels();
		chooser.getSelectionModel().addChangeListener(new SetBackgroundColourController(chooser, (Model_I_Controller) m));


		add(chooser, BorderLayout.CENTER);

		ccl = new DoneListener(this);

		JButton done = new JButton("Done");
		done.setFont(gf);
		done.setToolTipText("Click here when you are done choosing a colour");
		done.addActionListener(ccl);

		add(done, BorderLayout.PAGE_END);

	}

	/**
	 * @return chooser
	 * 			The colour chooser.
	 */
	public JColorChooser getChooser() {
		return chooser;
	}

	private void modifyPanels() 
	{
		AbstractColorChooserPanel[] panels = chooser.getChooserPanels();
		for (AbstractColorChooserPanel p : panels) 
		{
			if (!p.getDisplayName().equals("RGB")) 
			{
				chooser.removeChooserPanel(p);
			}
		}

		chooser.setPreviewPanel(new JPanel());

		final AbstractColorChooserPanel[] colorPanels = chooser.getChooserPanels();
		final AbstractColorChooserPanel cp = colorPanels[0];

		Field f = null;
		try {
			f = cp.getClass().getDeclaredField("panel");
		} catch (NoSuchFieldException | SecurityException e) {

		}
		f.setAccessible(true);

		Object colorPanel = null;
		try {
			colorPanel = f.get(cp);
		} catch (IllegalArgumentException | IllegalAccessException e) {

		}

		Field f2 = null;
		try {
			f2 = colorPanel.getClass().getDeclaredField("spinners");
		} catch (NoSuchFieldException | SecurityException e4) {

		}
		f2.setAccessible(true);
		Object rows = null;
		try {
			rows = f2.get(colorPanel);
		} catch (IllegalArgumentException | IllegalAccessException e3) {

		}

		final Object transpSlispinner = Array.get(rows, 3);
		Field f3 = null;
		try {
			f3 = transpSlispinner.getClass().getDeclaredField("slider");
		} catch (NoSuchFieldException | SecurityException e) {

		}
		f3.setAccessible(true);
		JSlider slider = null;
		try {
			slider = (JSlider) f3.get(transpSlispinner);
		} catch (IllegalArgumentException | IllegalAccessException e2) {

		}
		slider.setVisible(false);
		Field f4 = null;
		try {
			f4 = transpSlispinner.getClass().getDeclaredField("spinner");
		} catch (NoSuchFieldException | SecurityException e1) {

		}
		f4.setAccessible(true);
		JSpinner spinner = null;
		try {
			spinner = (JSpinner) f4.get(transpSlispinner);
		} catch (IllegalArgumentException | IllegalAccessException e) {

		}
		spinner.setVisible(false);
		Field f5 = null;
		try {
			f5 = transpSlispinner.getClass().getDeclaredField("label");
		} catch (NoSuchFieldException | SecurityException e1) {

		}
		f5.setAccessible(true);
		JLabel label = null;
		try {
			label = (JLabel) f5.get(transpSlispinner);
		} catch (IllegalArgumentException | IllegalAccessException e) {

		}
		label.setVisible(false);

		Field f6 = null;
		try {
			f6 = transpSlispinner.getClass().getDeclaredField("value");
		} catch (NoSuchFieldException | SecurityException e1) {

		}
		f6.setAccessible(false);
		try {
		} catch (IllegalArgumentException e) {

		}
	}
}
