package view;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.event.ActionListener;
import java.awt.event.ItemListener;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.awt.event.WindowListener;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.border.LineBorder;

import Model.Model_I_Controller;
import Model.Model_I_View;
import controller.GizmoController;
import controller.KeyConnectsController;
import controller.MagicKeyListener;
import controller.SwitchPanelController;
import controller.ViewListener;

/**
 * @author Adam Campbell
 * 
 */
public class RunGui
{
	/** The model we're working with */
	private Model_I_View model;
	/** KeyListener */
	private MagicKeyListener keyConnects;
	/** The frame of the system */
	private JFrame frame;
	/** Listeners */
	private ActionListener listener, aGControl;
	private ItemListener spListener;
	/** The board of the game */
	private Board board;
	/** Font of the system */
	private Font gf;
	/** Container */
	private Container cp;
	/** JPanels */
	private JPanel build, play, cards, comboBoxPane;
	/** Cards */
	final static String PLAY = "Play Mode";
	final static String BUILD = "Build Mode";
	/** Colour of the screen */
	private Color screenColour = Color.LIGHT_GRAY;
	/** Status bar */
	private String statusMessage = "Game ready to play";
	private Label status = new Label(statusMessage);

	/**
	 * Default constructor
	 * @param m
	 * 		The model
	 */
	public RunGui(Model_I_View m) 
	{
		// Instantiate
		model =  m;
		keyConnects = new MagicKeyListener(new KeyConnectsController((Model_I_Controller) model));
		listener = new ViewListener((Model_I_Controller) model);
	}

	/**
	 * Does what it says on the tin
	 */
	public void createAndShowGUI() {

		frame = new JFrame("Gizmoball W12");
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frame.setResizable(false);
		frame.setLocation(0, 0);
		frame.setVisible(true);

		frame.addWindowListener((WindowListener) listener);
		
		// Board is passed the Model so it can act as Observer
		board = new Board(481, 481, model);
		aGControl = new GizmoController(board, this, (Model_I_Controller) model);
		board.addMouseListener((MouseListener) aGControl);
		
		cp = frame.getContentPane();
		cp.addKeyListener(keyConnects);
		

		gf = new Font("Arial", Font.BOLD, 12);

		//Put the JComboBox in a JPanel to get a nicer look.
		comboBoxPane = new JPanel();
		comboBoxPane.setLayout(new BoxLayout(comboBoxPane, BoxLayout.PAGE_AXIS));
		String comboBoxItems[] = { PLAY, BUILD };

		JComboBox<String> cb = new JComboBox<String>(comboBoxItems);
		cb.setEditable(false);
		cb.setFont(gf);

		//Create the two possible left panels in separate methods to de-clutter
		createBuild();
		createPlay();

		//Create the panel that contains the "cards".
		cards = new JPanel(new CardLayout());
		cards.add(play, PLAY);
		cards.add(build, BUILD);

		spListener = new SwitchPanelController(this, board, (Model_I_Controller) model);
		cb.addItemListener(spListener);
		comboBoxPane.add(cb);
		cp.add(cards, BorderLayout.WEST);

		//Create the bottom bar which is consistent for both modes
		createBottomBar();


		//create message along top for button clicks
		createStatusBar();

		cp.add(board, BorderLayout.CENTER);

		frame.pack();

	}

	/**
	 * Create the build panel
	 */
	public void createBuild()
	{
		build = new JPanel();

		build.setBorder(new LineBorder(new Color(0, 0, 0)));
		build.setLayout(new BoxLayout(build, BoxLayout.PAGE_AXIS));
		build.setBackground(screenColour);
		build.setOpaque(true);
		
		JButton square = new JButton("Square");
		setButtonProperties(square, aGControl, 140, 100);
		square.setToolTipText("Select the coordinate where you want to place a Square");

		JButton circle = new JButton("Circle");
		setButtonProperties(circle, aGControl, 140, 100);
		circle.setToolTipText("Select the coordinate where you want to place a Circle");

		JButton triangle = new JButton("Triangle");
		setButtonProperties(triangle, aGControl, 140, 100);
		triangle.setToolTipText("Select the coordinate where you want to place a Triangle");

		JButton leftF = new JButton("Left Flipper");
		setButtonProperties(leftF, aGControl, 140, 100);
		leftF.setToolTipText("Select the coordinate where you want to place a Left Flipper");

		JButton rightF = new JButton("Right Flipper");
		setButtonProperties(rightF, aGControl, 140, 100);
		rightF.setToolTipText("Select the coordinate where you want to place a Right Flipper");

		JButton absorber = new JButton("Absorber");
		setButtonProperties(absorber, aGControl, 140, 100);
		absorber.setToolTipText("Select the coordinate where you want to place the Top left corner of a Absorber");

		JButton ball = new JButton("Ball");
		setButtonProperties(ball, aGControl, 140, 100);
		ball.setToolTipText("Select the coordinate where you want to place a Ball");

		JButton move = new JButton("Move");
		setButtonProperties(move, aGControl, 140, 100);
		move.setToolTipText("Select the Gizmo you wish to move, then the coordinate where you wish to place it");

		JButton rotate = new JButton("Rotate");
		setButtonProperties(rotate, aGControl, 140, 100);
		rotate.setToolTipText("Select the Gizmo you wish to rotate");

		JButton delete = new JButton("Delete");
		setButtonProperties(delete, aGControl, 140, 100);
		delete.setToolTipText("Select the Gizmo you wish to delete");

		JButton connect = new JButton("Connect");
		setButtonProperties(connect, aGControl, 140, 100);
		connect.setToolTipText("Select the Gizmo you wish to connect");

		JButton keyConnect = new JButton("Key Connect");
		setButtonProperties(keyConnect, aGControl, 140, 100);
		keyConnect.addKeyListener((KeyListener) aGControl);
		keyConnect.setToolTipText("Select the Gizmo you wish to connect a key to");

		JButton setColour = new JButton("Set Colour");
		setButtonProperties(setColour, aGControl, 140, 100);
		setColour.setToolTipText("Select the Gizmo you wish to change the colour of");

		JButton setFriction = new JButton("Set Friction");
		setButtonProperties(setFriction, aGControl, 140, 100);
		setFriction.setToolTipText("Set the Friction of the Game");

		JButton setGravity = new JButton("Set Gravity");
		setButtonProperties(setGravity, aGControl, 140, 100);
		setGravity.setToolTipText("Set the Gravity of the Game");

		JSeparator j1 = new JSeparator();

		//add buttons in the order they should appear
		build.add(square); build.add(circle); build.add(triangle); build.add(leftF);
		build.add(rightF); build.add(absorber); build.add(ball);

		build.add(j1);

		build.add(move); build.add(rotate); build.add(delete); build.add(connect);
		build.add(keyConnect); build.add(setColour); build.add(setFriction); build.add(setGravity);
	}

	/**
	 * Create the play panel
	 */
	public void createPlay()
	{
		play = new JPanel();
		play.setLayout(new GridLayout(4,1));

		play.setBackground(screenColour);
		play.setOpaque(true);
		
		JLabel logo = new JLabel();
		logo.setIcon(new ImageIcon("logos/logo.png"));

		JButton run = new JButton("Play");
		setButtonProperties(run, listener, 100, 100);
		run.addKeyListener(keyConnects);
		run.setIcon(new ImageIcon("logos/play.png"));

		JButton stop = new JButton("Stop");
		setButtonProperties(stop, listener, 100, 100);
		stop.setIcon(new ImageIcon("logos/black-square.jpg"));

		JButton tick = new JButton("Tick");
		setButtonProperties(tick, listener, 100, 100);

		JButton reset = new JButton("Reset");
		setButtonProperties(reset, listener, 100, 100);
		reset.setIcon(new ImageIcon("logos/refresh.png"));

		//add buttons in the order they should appear
		play.add(logo); play.add(run); play.add(stop); play.add(reset);
	}

	/**
	 * Create the bottom bar
	 */
	private void createBottomBar()
	{
		JPanel bottom = new JPanel();
		bottom.setLayout(new GridLayout(2, 5));	
		bottom.setBackground(screenColour);
		bottom.setOpaque(true);
		bottom.add(comboBoxPane);
		
		JButton newG = new JButton("New Game");
		setButtonProperties(newG, listener, 100, 100);

		JButton save = new JButton("Save Game");
		setButtonProperties(save, listener, 100, 100);

		JButton load = new JButton("Load Game");
		setButtonProperties(load, listener, 100, 100);

		JButton setBackground = new JButton("Set Background");
		setButtonProperties(setBackground, listener, 100, 100);

		JButton quit = new JButton("Quit");
		setButtonProperties(quit, listener, 100, 100);

		//add buttons in the order they should appear
		bottom.add(newG); bottom.add(save); bottom.add(load);
		bottom.add(setBackground); bottom.add(quit);

		cp.add(bottom, BorderLayout.SOUTH);
	}


	/**
	 * Create the status bar 
	 */
	private void createStatusBar()
	{	
		status.setFont(gf);
		status.setPreferredSize(new Dimension(500, 15));
		status.setAlignment(Label.CENTER);
		
		JPanel statusBar = new JPanel();
		statusBar.setLayout(new FlowLayout(FlowLayout.CENTER));
		statusBar.setSize(frame.getSize().width, 10);
		statusBar.add(status);
		cp.add(statusBar, BorderLayout.NORTH);

	}

	/**
	 * @param s
	 * 		The {@link String} to set the status bar too
	 */
	public void setStatusBar(String s) 
	{
		status.setText(s);
	}

	/**
	 * @param mess
	 * 		The {@link String} to display
	 */
	public void displayMessage(String mess) {
		JOptionPane.showMessageDialog(null, mess);
	}

	/**
	 * @param c
	 * 		The card to display
	 */
	public void setCard(String c) {
		CardLayout cl = (CardLayout) cards.getLayout();
		cl.show(cards, c);
	}

	/**
	 * @param b
	 * @param l
	 * @param x
	 * @param y
	 * 		Helper method to neaten creation of buttons
	 */
	private void setButtonProperties(JButton b, ActionListener l , int x , int y)
	{
		b.setFont(gf);
		b.setMaximumSize(new Dimension(x, y));
		b.addActionListener(l);
	}
}
