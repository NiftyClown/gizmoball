package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;

import Model.Model_I_Controller;
import Model.Model_I_View;
import controller.DoneListener;
import controller.SetGravityController;

public class GravitySlider extends JFrame 
{
	private static final long serialVersionUID = 8006070997735957640L;
	static final int G_MIN = -30;
	static final int G_MAX = 30;
	static final int G_INIT = 0;
	/** Font of the class */
	private Font gf, mediumgf, smallgf;
	/** The model we're working */
	@SuppressWarnings("unused")
	private Model_I_View gm;
	/** The done listener */
	private DoneListener gl;

	public GravitySlider(Model_I_View m)
	{
		this.setSize(new Dimension(620,250));
		this.setLocation(640, 0);
		this.setTitle("Set Gravity");
		this.gm = m;

		gf = new Font("Arial", Font.BOLD, 30);
		mediumgf = new Font("Arial", Font.BOLD, 16);
		smallgf = new Font("Arial", Font.BOLD, 12);

		// Set a background for JFrame
		setContentPane(new JLabel(new ImageIcon("logos/gravity.png")));

		// Set the layout
		setLayout(new BorderLayout());

		//Create the slider.
		JSlider gravity = new JSlider(JSlider.HORIZONTAL,G_MIN, G_MAX, G_INIT);
		setSliderProperties(gravity, 10, 1);
		gravity.setForeground(Color.GREEN);
		gravity.addChangeListener(new SetGravityController((Model_I_Controller) m, gravity));

		gl = new DoneListener(this);

		//Add button for user finishing
		JButton done = new JButton("Done");
		done.setFont(smallgf);
		done.setToolTipText("Click here when you are done choosing a colour");
		done.addActionListener(gl);

		//create labels for sliders
		JLabel xf = new JLabel("Change Force of Gravity", JLabel.CENTER);
		xf.setAlignmentX(Component.CENTER_ALIGNMENT);
		xf.setFont(mediumgf);
		xf.setForeground(Color.RED);
		JLabel yf = new JLabel("Change Y Friction", JLabel.CENTER);
		yf.setFont(mediumgf);
		yf.setForeground(Color.RED);

		//create panels 
		JPanel top = new JPanel();
		top.setPreferredSize(new Dimension(this.getWidth(), 200));
		top.setOpaque(false);
		top.add(xf, BorderLayout.NORTH);
		top.add(gravity, BorderLayout.SOUTH);

		//add to frame
		add(top, BorderLayout.CENTER);
		add(done, BorderLayout.SOUTH);
	}

	/**
	 * @param js
	 * @param x
	 * @param y
	 * 		Helper method to neaten creation of sliders
	 */
	public void setSliderProperties(JSlider js, int x, int y)
	{
		js.setPaintTicks(true);
		js.setPaintLabels(true);
		js.setFont(gf);
		js.setMajorTickSpacing(x);
		js.setMinorTickSpacing(y);
		js.setOpaque(false);
		js.setPreferredSize(new Dimension(600,100));
	}
}
