package controller;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import Model.Model_I_Controller;

/**
 * Holds a list of connected keys.
 * When the key's pressed tells the model and makes stuff happen.
 * @author Nathan
 *
 */
public class KeyConnectsController implements KeyListener {

	/** The model we're working with */
	private Model_I_Controller model;

	/**
	 * @param model The model that we're messing with.
	 */
	public KeyConnectsController(Model_I_Controller model) {
		this.model = model;
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}

	@Override
	public void keyPressed(KeyEvent e) {		
		if(model.getKeyDownConnects().contains(e.getKeyCode())){
			model.action(e.getKeyCode(), "down");
		}		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		if(model.getKeyUpConnects().contains(e.getKeyCode())){
			model.action(e.getKeyCode(), "up");
		}
	}
}