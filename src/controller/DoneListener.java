package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import view.ColourChooser;
import view.FrictionSlider;
import view.GravitySlider;

public class DoneListener implements ActionListener
{
	private ColourChooser cc;
	private FrictionSlider fs;
	private GravitySlider gs;
	private String command;
	
	public DoneListener(ColourChooser c)
	{
		cc = c;
		command = "c";
	}
	
	public DoneListener(FrictionSlider f)
	{
		fs = f;
		command = "f";
	}
	
	public DoneListener(GravitySlider g)
	{
		gs = g;
		command = "g";
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		// Assertion
		assert e != null : "Error: Something is wrong with action events from the view";

		switch (e.getActionCommand()) 
		{
		case "Done": done();
			break;
		
		}
		
	}

	private void done() 
	{
		if (command == "c")
			cc.setVisible(false);
		if (command == "f")
			fs.setVisible(false);
		if (command == "g")
			gs.setVisible(false);
	}

}
