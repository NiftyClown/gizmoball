package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JSlider;

import view.SpeedChooser;
import Model.Model_I_Controller;

public class SpeedController implements ActionListener {

	/** The model */
	private Model_I_Controller model;
	/** JSliders */
	private JSlider js1, js2;
	/** Coordinates */
	private int gridX, gridY;
	/** The speed chooser */
	private SpeedChooser sc;

	/**
	 * Default constructor.
	 * 
	 * @param m
	 * 		The model
	 * @param x
	 * 		X coordinate the ball is going to be placed
	 * @param y
	 * 		Y coordinate the ball is going to be placed
	 * @param s1
	 * 		X speed slider
	 * @param s2
	 * 		Y speed slider
	 * @param chooser
	 * 		The frame we're deconstructing
	 */
	public SpeedController(Model_I_Controller m, int x, int y, JSlider s1, JSlider s2, SpeedChooser chooser) {
		model = m;
		gridX = x;
		gridY = y;
		js1 = s1;
		js2 = s2;
		sc = chooser;
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		sc.dispose();
		model.addBall("", gridX, gridY, js1.getValue(), js2.getValue());
	}

}
