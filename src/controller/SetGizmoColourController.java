package controller;

import javax.swing.JColorChooser;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import Model.Model_I_Controller;

public class SetGizmoColourController implements ChangeListener
{
	private Model_I_Controller model;
	private JColorChooser jcc;
	private int x, y;

	public SetGizmoColourController(JColorChooser c, Model_I_Controller m, int gridX, int gridY) {
		model = m;
		jcc = c;
		x = gridX;
		y = gridY;
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		model.setGizmoColour(x, y, jcc.getColor().getRGB());
	}
}