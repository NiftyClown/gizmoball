package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import view.Board;
import view.ColourChooser;
import view.FrictionSlider;
import view.GravitySlider;
import view.RunGui;
import view.SpeedChooser;
import Model.GameLoop;
import Model.Model_I_Controller;
import Model.Model_I_View;

public class GizmoController implements ActionListener, MouseListener,
		KeyListener {
	/** The visual representation of the model */
	private Board board;
	/** Strings used for operations */
	private String choice, gizmoName;
	/** The model */
	private Model_I_Controller model;
	/** Parent frame of the view component */
	private RunGui rG;
	/** Integer variables associated with the placement of absorbers */
	private int absorber1x, absorber1y;
	/** Boolean associated with connecting */
	private boolean connecting;
	/** String used for carrying over Gizmos in two operations */
	private String gizmo1;
	/** Controller for setting colour of Gizmos */
	@SuppressWarnings("unused")
	private ColourChooser cc;
	/** The speed chooser */
	private SpeedChooser speedch;
	/** Friction slider */
	private FrictionSlider fs;
	/** Gravity slider */
	private GravitySlider gs;

	/**
	 * Default constructor.
	 * 
	 * @param b
	 *            The current {@link Board}
	 * @param r
	 *            The current {@link RunGui}
	 * @param m
	 *            The current {@link Model_I_Controller}
	 */
	public GizmoController(Board b, RunGui r, Model_I_Controller m) {

		connecting = false;
		choice = "";
		board = b;
		model = m;
		rG = r;
		
		fs = new FrictionSlider((Model_I_View) model);
		gs = new GravitySlider((Model_I_View) model);
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		// Assertion
		assert e != null : "Error: Something is wrong with action events from the view";

		switch (e.getActionCommand()) {
		case "Square":
			addSquare();
			break;
		case "Circle":
			addCircle();
			break;
		case "Triangle":
			addTriangle();
			break;
		case "Left Flipper":
			addLeftF();
			break;
		case "Right Flipper":
			addRightF();
			break;
		case "Absorber":
			addAbsorber();
			break;
		case "Ball":
			addBall();
			break;
		case "Move":
			move();
			break;
		case "Rotate":
			rotate();
			break;
		case "Delete":
			delete();
			break;
		case "Connect":
			connect();
			break;
		case "Key Connect":
			keyConnect();
			break;
		case "Set Colour":
			setColour();
			break;
		case "Set Friction":
			setFriction();
			break;
		case "Set Gravity":
			setGravity();
			break;
		}
	}

	/** Allocates the correct variables depending on the choice */

	private void addSquare() {
		rG.setStatusBar("Select the coordinate where you want to place a Square");
		choice = "Square";
	}

	private void addCircle() {
		rG.setStatusBar("Select the coordinate where you want to place a Circle");
		choice = "Circle";
	}

	private void addTriangle() {
		rG.setStatusBar("Select the coordinate where you want to place a Triangle");
		choice = "Triangle";
	}

	private void addLeftF() {
		rG.setStatusBar("Select the coordinate where you want to place a Left Flipper");
		choice = "LeftFlipper";
	}

	private void addRightF() {
		rG.setStatusBar("Select the coordinate where you want to place a Right Flipper");
		choice = "RightFlipper";
	}

	private void addAbsorber() {
		rG.setStatusBar("Select the two coordinates between which you wish to add an Absorber");
		choice = "Absorber";
	}

	private void addBall() {
		rG.setStatusBar("Select the coordinate where you want to place a Ball");
		choice = "Ball";
	}

	private void move() {
		rG.setStatusBar("Select the Gizmo you wish to move and then the place where you wish to move it to");
		choice = "Move";
	}

	private void rotate() {
		rG.setStatusBar("Select the Gizmo you wish to rotate");
		choice = "Rotate";
	}

	private void delete() {
		rG.setStatusBar("Select the Gizmo you wish to delete");
		choice = "Delete";
	}

	private void connect() {
		rG.setStatusBar("Select the two Gizmos you wish to connect");
		choice = "Connect";
	}

	private void keyConnect() {
		rG.setStatusBar("Select the Gizmo you wish to add a Key Connect too");
		choice = "KeyConnect";
	}

	private void setColour() {
		rG.setStatusBar("Select the Gizmo you wish to change the colour of");
		choice = "Colour";
	}

	private void setFriction() {
		rG.setStatusBar("Select the effect of Friction");
		fs.setVisible(true);
	}
	
	private void setGravity() {
		rG.setStatusBar("Select the effect of Gravity");
		gs.setVisible(true);
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if (e.getPoint() != null && board.getPanelTF()) {
			int xcoord = e.getX();
			int ycoord = e.getY();

			// Final logical co-ordinates
			int gridX = xcoord / GameLoop.getL();
			int gridY = ycoord / GameLoop.getL();

			try {
				switch (choice) {
				case "Square":
					model.addGizmo("Square", "", gridX, gridY);
					break;
				case "Circle":
					model.addGizmo("Circle", "", gridX, gridY);
					break;
				case "Triangle":
					model.addGizmo("Triangle", "", gridX, gridY);
					break;
				case "LeftFlipper":
					try {
						model.addGizmo("LeftFlipper", "", gridX, gridY);
					} catch (IndexOutOfBoundsException ie) {
						rG.displayMessage("Error: Out of bounds!");
					}
					break;
				case "RightFlipper":
					try {
						model.addGizmo("RightFlipper", "", gridX, gridY);
					} catch (IndexOutOfBoundsException ie) {
						rG.displayMessage("Error: Out of bounds!");
					}
					break;
				case "Ball":
					speedch = new SpeedChooser((Model_I_View) model, gridX,
							gridY);
					speedch.setVisible(true);
					break;
				case "Absorber":
					absorber1x = gridX;
					absorber1y = gridY;
					choice = "Absorber2";
					break;
				case "Absorber2":
					// Values
					int absorber2x = gridX;
					int absorber2y = gridY;
					int width = absorber2x - absorber1x;
					int height = absorber2y - absorber1y;

					// Error checking
					if (width >= 0 && height >= 0) {
						model.addAbsorber("", absorber1x, absorber1y,
								width + 1, height + 1);
					} else if (width < 0 && height > 0) {
						model.addAbsorber("", absorber2x, absorber1y,
								(Math.abs(width) + 1), height + 1);
					} else if (width > 0 && height < 0) {
						model.addAbsorber("", absorber1x, absorber2y,
								width + 1, (Math.abs(height) + 1));
					} else {
						model.addAbsorber("", absorber2x, absorber2y,
								(Math.abs(width) + 1), (Math.abs(height) + 1));
					}
					choice = "Absorber";
					break;
				case "Rotate":
					model.rotate(model.gizmoAt(gridX, gridY));
					break;
				case "Delete":
					model.delete(model.gizmoAt(gridX, gridY));
					break;
				case "KeyConnect":
					gizmoName = model.gizmoAt(gridX, gridY);
					connecting = true;
					rG.setStatusBar("Press the key you want to connect to");
					break;
				case "Connect":
					gizmo1 = model.gizmoAt(gridX, gridY);
					choice = "Connect2";
					rG.setStatusBar("Click on the Gizmo to connect to");
					break;
				case "Connect2":
					model.connect(gizmo1, model.gizmoAt(gridX, gridY));
					choice = "Connect";
					rG.setStatusBar("Select the two Gizmos you wish to connect");
					break;
				case "Move":
					gizmo1 = model.gizmoAt(gridX, gridY);
					choice = "Move2";
					rG.setStatusBar("Click on the position to move this gizmo to");
					break;
				case "Move2":
					if (model.move(gizmo1, gridX, gridY)) {

					} else {
						rG.displayMessage("Error: This spot is occupied");
					}
					choice = "Move";
					break;
				case "Colour":
					if (model.gizmoAt(gridX, gridY) == null)
						rG.displayMessage("Error: No Gizmo here");
					else
						cc = new ColourChooser((Model_I_View) model, gridX,
								gridY);
					break;
				}

			} catch (NullPointerException ne) {
				// Do nothing
			}
		}
	}

	/**
	 * Connect the selected gizmo to the desired key
	 */
	@Override
	public void keyPressed(KeyEvent e) {
		if (connecting) {
			model.keyConnect(e.getKeyCode(), "up", gizmoName);
			if (gizmoName.contains("Flipper"))
				model.keyConnect(e.getKeyCode(), "down", gizmoName);

			rG.setStatusBar("Key pressed: " + e.getKeyChar());
			connecting = false;
		}
	}

	// unrequired methods
	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}

	@Override
	public void keyReleased(KeyEvent e) {
	}
}
