package controller;

import java.util.ArrayList;
import java.util.Scanner;

import Model.Model_I_Controller;

/**
 * Validates the input file and creates the required object if the file is
 * valid.
 * 
 * @author Mark & Nathan
 * 
 */

public class Parser {

	/**
	 * The regex syntax that will be used to define the validation of the input
	 * file. If the input line matches one of the below rules then that line is
	 * valid, else the line is invalid Syntax for the parser
	 */
	private static final String GIZMO = "((Square)|(Circle)|(LeftFlipper)|(RightFlipper)|(Triangle)) [a-zA-Z\\d_]+ \\d+ \\d+\\s*";
	private static final String BALL = "Ball [\\da-zA-Z_]* \\d+.\\d+\\s* \\d+.\\d+\\s* -?\\d+.\\d+\\s* -?\\d+.\\d+\\s*";
	private static final String KEYCONNECT = "KeyConnect key \\d+\\s* [\\da-zA-Z_]+\\s* [\\da-zA-Z]+\\s*";
	private static final String ABSORBER = "Absorber [\\da-zA-Z_]+ \\d+ \\d+ \\d+ \\d+\\s*";
	private static final String OPS = "((Delete)|(Rotate)) [\\da-zA-Z_]+\\s*";
	private static final String CONNECT = "Connect [\\da-zA-Z_]+ [\\da-zA-Z_]+\\s*";
	private static final String GRAVITY = "Gravity \\d+\\.\\d+\\s*";
	private static final String FRICTION = "Friction \\d+.\\d+\\s+\\d+.\\d+\\s*";
	private static final String MOVEDEC = "Move [a-zA-Z\\d_]+ \\d+\\.?\\d+ \\d+\\.?\\d+\\s*";
	private static final String MOVE = "Move [a-zA-Z\\d_]+ \\d+ \\d+\\s*";
	private static final String COLOUR = "Colour [a-zA-Z\\d_]+ -?\\d+\\s*";

	/** The source of the file we're reading */
	private ArrayList<String> source;
	/** The model */
	private Model_I_Controller model;

	/**
	 * Default constructor.
	 * 
	 * @param source
	 *            The source of the file
	 * @param model
	 *            The model
	 */
	protected Parser(ArrayList<String> source, Model_I_Controller model) {
		this.source = source;
		this.model = model;
	}

	/**
	 * Read through the file validating it line by line using the above regex
	 * rules.
	 * 
	 * @throws Exception
	 */
	protected void validate() throws Exception {
		for (int i = 0; i < source.size(); i++) {
			if (source.get(i).matches(GIZMO)) {
				;
			} else if (source.get(i).matches(BALL)) {
				;
			} else if (source.get(i).matches(KEYCONNECT)) {
				;
			} else if (source.get(i).matches(ABSORBER)) {
				;
			} else if (source.get(i).matches(OPS)) {
				;
			} else if (source.get(i).matches(CONNECT)) {
				;
			} else if (source.get(i).matches(GRAVITY)) {
				;
			} else if (source.get(i).matches(FRICTION)) {
				;
			} else if (source.get(i).matches(MOVE)) {
				;
			} else if (source.get(i).matches(MOVEDEC)) {
				;
			} else if (source.get(i).matches(COLOUR)) {
				;
			} else {
				System.out.println("Invalid line: '" + source.get(i) + "'");
				throw new Exception("Error on line number: " + (i + 1));
			}
		}
	}

	/**
	 * Once the file has been validated, each line from the file will be be read
	 * through to create the desired object dependant on the first word in that
	 * line. The objects are created in the model. Passes in the file to the
	 * model.
	 */
	protected void createObject() {
		String input;
		for (int i = 0; i < source.size(); i++) {
			Scanner s = new Scanner(source.get(i));
			input = s.next();
			if (input.equalsIgnoreCase("Triangle")
					|| input.equalsIgnoreCase("Square")
					|| input.equalsIgnoreCase("Circle")
					|| input.equalsIgnoreCase("LeftFlipper")
					|| input.equalsIgnoreCase("RightFlipper")) {
				/** Type, variable name, x grid coordinate, y grid coordinate */
				model.addGizmo(input, s.next(), s.nextInt(), s.nextInt());
			} else if (input.equalsIgnoreCase("Connect")) {
				/** Variable names of the gizmos that have to be connected */
				model.connect(s.next(), s.next());
				continue;
			} else if (input.equalsIgnoreCase("KeyConnect")) {
				/**
				 * int key = key number for the letter pressed String name =
				 * gizmo that it is being connected to String dir = direction
				 */
				s.next(); // Skip over this stuffs as "key" is not required for
							// parsing
				int key = s.nextInt();
				String dir = s.next();
				String name = s.next();
				model.keyConnect(key, dir, name);
				continue;
			} else if (input.equalsIgnoreCase("Absorber")) {
				/**
				 * Variable name, x grid coordinate and y grid coordinate for
				 * top left and x grid coordinate and y grid coordinate for
				 * bottom right
				 */
				model.addAbsorber(s.next(), s.nextInt(), s.nextInt(),
						s.nextInt(), s.nextInt());
				continue;
			} else if (input.equalsIgnoreCase("Move")) {
				/** Variable name of the gizmo that is to be moved */
				model.move(s.next(), s.nextInt(), s.nextInt());
				continue;
			} else if (input.equalsIgnoreCase("Rotate")) {
				/** Variable name of the gizmo that is to be rotated */
				model.gizmoAt(s.next()).rotate();
				continue;
			} else if (input.equalsIgnoreCase("Delete")) {
				/** Variable name of the gizmo that is to be deleted */
				model.delete(s.next());
				continue;
			} else if (input.equalsIgnoreCase("Ball")) {
				model.addBall(s.next(), s.nextDouble(), s.nextDouble(),
						s.nextDouble(), s.nextDouble());
				continue;
			} else if (input.equals("Gravity")) {
				model.setGravity(s.nextFloat());
				continue;
			} else if (input.equals("Friction")) {
				model.setFriction(s.nextFloat(), s.nextFloat());
				continue;
			} else if (input.equals("Colour")) {
				model.setGizmoColour(s.next(), s.nextInt());
				continue;
			} else {
				System.out.println("Broken :(");
			}
			
			s.close();
		}
	}
	
}