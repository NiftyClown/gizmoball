package controller;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import view.Board;
import view.RunGui;
import Model.Model_I_Controller;

public class SwitchPanelController implements ItemListener  {

	/** Easy modular boolean value to switch */
	private boolean panel;
	/** The model we're working with */
	private Model_I_Controller model; 
	/** The parent RunGUI */
	private RunGui rg;
	/** The game visual representation */
	private Board board;

	/**
	 * Default constructor.
	 * @param rung
	 * 		RunGui
	 * @param b
	 * 		The current board
	 * @param m
	 * 		The current model
	 */
	public SwitchPanelController(RunGui rung, Board b, Model_I_Controller m) {
		rg = rung;
		board = b;
		model = m;
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		switch (e.getStateChange()) 
		{
		case ItemEvent.DESELECTED:
			break;
		case ItemEvent.SELECTED: 

			if (e.getItem().toString().equals("Build Mode"))  {
				ViewListener.timer.stop();
				model.reset();
				rg.setStatusBar("Set up your Game");
			}
			if (e.getItem().toString().equals("Play Mode"))  {
				rg.setStatusBar("Game ready to Play");
			}

			rg.setCard(e.getItem().toString());
			setPanel();
			board.repaint();

			break;
		}
	}

	/**
	 * Rotate the boolean value of panel
	 */
	private void setPanel(){	
		this.panel = !(this.panel);
		board.setPanelTF(panel);
	}
}