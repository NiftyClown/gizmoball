package controller;

import java.io.File;
import java.io.IOException;

import javax.swing.JFileChooser;

import Model.Model_I_Controller;

public class LoaderController {

	/** The object to read the file */
	private FileRead fileRead;
	/** The model we're working with */
	private Model_I_Controller m;
	/** The file chooser */
	private JFileChooser fc;

	/**
	 * Constructor for the class.
	 * @param m
	 * 		The model
	 */
	public LoaderController(Model_I_Controller m) {
		this.m = m;
		this.Load();
	}

	/**
	 * Loads the file.
	 */
	private void Load() {
		fileRead = new FileRead(m);
		try {
			this.openFile();
		} catch (IOException e) {
			System.out.println("Not Working");
		}
	}

	/**
	 * @throws IOException User selects a file in the GUI.
	 * Selected file is loaded in the readFile() method.
	 */
	private void openFile() throws IOException {
		fc = new JFileChooser();
		int returnVal = fc.showOpenDialog(fc);

		if (returnVal == JFileChooser.APPROVE_OPTION) 
		{
			File file = fc.getSelectedFile();
			fileRead.readFile(file);
		}
	}
}