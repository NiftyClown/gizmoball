package controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import Model.Model_I_Controller;

/**
 * Loads the user selected file onto the board
 * @author Mark
 *
 */

public class FileRead {

	/** The model we're working with */
	private Model_I_Controller m;
	/** The parser for the file */
	private Parser tsp;
	/** The {@link List} of all data in the file */
	private ArrayList<String> arr;

	/**
	 * Constructor for the class
	 * 
	 * @param model
	 */
	protected FileRead(Model_I_Controller model){
		this.m = model;
	}

	/**
	 * @param fileName 
	 * A File is passed in from the openFile() method, selected from the user using the 'Load Game' button on the GUI.
	 * File is read through line at a time. Each line is passed through the Parser as a string to be validated.
	 * Only if the file is valid, will the objects then be created and placed on the board.
	 */
	protected void readFile(File fileName) {
		BufferedReader br = null;
		String line;
		String input;
		arr = new ArrayList<String>();
		m.clear();

		try{
			br = new BufferedReader(new FileReader(fileName));
			while((line = br.readLine()) != null){
				StringTokenizer st = new StringTokenizer(line, "\n");
				if(!st.hasMoreTokens()) {
					continue;
				}
				input = st.nextToken();
				arr.add(input + "\n");
			}
			br.close();
			tsp = new Parser(arr, m);
			tsp.validate();
			tsp.createObject();
		} catch(Exception e){
			System.out.println("Invalid File. " + e);
		}
	}
}