package controller;

import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import Model.Model_I_Controller;

public class SetFrictionController implements ChangeListener
{
	/** The model we're working */
	private Model_I_Controller gm;
	/** The JSliders */
	private JSlider js1, js2;

	/**
	 * Default constructor.
	 * 
	 * @param m
	 * 		The model we're working with
	 */
	public SetFrictionController(Model_I_Controller m, JSlider s1, JSlider s2) {
		gm = m;
		js1 = s1;
		js2 = s2;
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		gm.setFriction((float) js1.getValue()/1000, (float)js2.getValue()/1000);
	}
}