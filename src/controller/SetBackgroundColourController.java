package controller;

import javax.swing.JColorChooser;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import Model.Model_I_Controller;

public class SetBackgroundColourController implements ChangeListener
{
	private Model_I_Controller model;
	private JColorChooser jcc;

	public SetBackgroundColourController(JColorChooser c, Model_I_Controller m) {
		model = m;
		jcc = c;
	}

	public void stateChanged(ChangeEvent e) {
		model.setBackgroundColour(jcc.getColor().getRGB());
	}
}
