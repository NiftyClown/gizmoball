package controller;

import javax.swing.SwingUtilities;

import view.SplashScreen;

public class Task extends Thread 
{    
	SplashScreen splash;
	
    public Task(SplashScreen s)
    {
    	splash = s;
    }
 

    public void run(){
       for(int i =0; i<= 100; i+=5){
          final int progress = i;
          SwingUtilities.invokeLater(new Runnable() 
          {
             public void run() 
             {
            	 splash.setProgress(progress); 
             }
          });
          try {
             Thread.sleep(140);
          } catch (InterruptedException e) {}
       }
    }
 }   
