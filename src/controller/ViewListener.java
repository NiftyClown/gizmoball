package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.Timer;

import view.ColourChooser;
import Model.Model_I_Controller;
import Model.Model_I_View;

/**
 * @author Adam Campbell based on Murray's MVC Code
 */

public class ViewListener implements ActionListener, WindowListener {

	/** The timer the model is working off */
	protected static Timer timer;
	/** The model we're working with */
	private Model_I_Controller model;
	private ColourChooser cc;

	/**
	 * Default constructor for the class.
	 * @param m
	 */
	public ViewListener(Model_I_Controller m) {
		model = m;
		cc = new ColourChooser((Model_I_View) model);
		timer = new Timer(5, this);
	}

	@Override
	public final void actionPerformed(final ActionEvent e) {
		if (e.getSource() == timer) {
			model.moveBall();
		} else {
			String choice = e.getActionCommand();
			// call methods based on which button is pressed
			switch (choice) {
			case "Play":
				timer.start();
				break;
			case "Stop":
				timer.stop();
				break;
			case "Tick":
				model.moveBall();
				break;
			case "Reset" :
				model.reset();
				timer.stop();
				break;
			case "New Game":
				model.clear();
				break;	
			case "Load Game":
				@SuppressWarnings("unused")
				LoaderController load = new LoaderController(model);
				break;
			case "Save Game":
				JFileChooser chooser = new JFileChooser();
				chooser.showSaveDialog(chooser);
				model.writeFile(chooser.getSelectedFile());
				break;
			case "Quit":
				quit();
				break;
			case "Set Background":
				cc.setVisible(true);
				break;
			}
		}
	}


	private void quit() {
		int exitResult = JOptionPane
				.showConfirmDialog(
						null,
						"Are you sure you wish to exit? \n (Unsaved changes will be lost)",
						"Exit", JOptionPane.YES_NO_OPTION);
		if (exitResult == JOptionPane.YES_OPTION) {
			System.exit(0);
		}
	}


	@Override
	public void windowClosing(WindowEvent e) 
	{
		int exitResult = JOptionPane
				.showConfirmDialog(
						null,
						"Are you sure you wish to exit? \n (Unsaved changes will be lost)",
						"Exit", JOptionPane.YES_NO_OPTION);
		if (exitResult == JOptionPane.YES_OPTION) {
			System.exit(0);
		}
	}
	
	//Unused Methods
	@Override
	public void windowOpened(WindowEvent e) {
	}
	@Override
	public void windowClosed(WindowEvent e) {
	}
	@Override
	public void windowIconified(WindowEvent e) {
	}
	@Override
	public void windowDeiconified(WindowEvent e) {
	}
	@Override
	public void windowActivated(WindowEvent e) {
	}
	@Override
	public void windowDeactivated(WindowEvent e) {
	}
}