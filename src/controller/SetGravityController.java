package controller;

import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import Model.Model_I_Controller;

public class SetGravityController implements ChangeListener
{
	/** The model we're working */
	private Model_I_Controller gm;
	/** The slider */
	private JSlider slider;

	/**
	 * Default constructor.
	 * 
	 * @param m
	 * 		The model we're working with
	 */
	public SetGravityController(Model_I_Controller m, JSlider s) {
		gm = m;
		slider = s;
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		gm.setGravity(slider.getValue());
	}
}