package UnitTests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import Model.GameLoop;
import Model.Model_I;

public class GameLoopTest {
	
	/** The model we're manipulating */
	Model_I model;

	@Before
	public void setUp() throws Exception {
		model = new GameLoop();
	}

	@Test
	public void testAddGizmo() {
		// Add in all types of Gizmos
		assertTrue(model.addGizmo("Square", "S", 0, 0));
		assertTrue(model.addGizmo("Circle", "C", 0, 1));
		assertTrue(model.addGizmo("Triangle", "T", 0, 2));
		assertTrue(model.addGizmo("LeftFlipper", "LF", 0, 3));
		assertTrue(model.addGizmo("RightFlipper", "RF", 5, 6));
		
		// Adding in the same position as a Gizmo
		assertFalse(model.addGizmo("Circle", "c2", 0, 1));
		
		// Null check
		assertFalse(model.addGizmo(null, null, 0, 9));
	}

	@Test
	public void testConnect() {
		// Add in two gizmos
		assertTrue(model.addGizmo("Square", "S", 0, 0));
		assertTrue(model.addGizmo("Circle", "C", 0, 1));
		
		// Connect them both to one another
		assertTrue(model.connect("S", "C"));
		assertTrue(model.connect("C", "S"));
		
		// Add the same connect again, it should be false...
		assertFalse(model.connect("C", "S"));
		
		// Null check
		assertFalse(model.connect(null, null));
	}

	@Test
	public void testKeyConnect() {
		// Add in gizmos
		assertTrue(model.addGizmo("Square", "S", 0, 0));

		// Connect them both to one another
		assertTrue(model.keyConnect(43, "up", "S"));
		assertTrue(model.keyConnect(43, "down", "S"));

		// Null check
		assertFalse(model.keyConnect(43, null, null));
	}

	@Test
	public void testMove() {
		// Add in two gizmos
		assertTrue(model.addGizmo("Square", "S", 0, 0));
		assertTrue(model.addGizmo("Circle", "C", 0, 1));
		
		// Move one
		assertTrue(model.move("S", 0, 2));
		
		// Try to move the other one on top...
		assertFalse(model.move("C", 0, 2));
		
		// It should still be there
		assertTrue(model.gizmoAt(0, 1).equals("C"));
		
		// Null check
		assertFalse(model.move(null, 0, 0));
	}

	@Test
	public void testAddBall() {
		// Adding in a ball
		assertTrue(model.addBall("B",10.0,10.0,50,5.0));
		
		// Attempting to add more than one ball
		assertFalse(model.addBall("B",5.0,5.0,50,5.0));
		
		// Null ball
		assertFalse(model.addBall(null, 0, 0, 0, 0));
	}

	@Test
	public void testAddAbsorber() {
		// Adding an absorber
		assertTrue(model.addAbsorber("A", 0, 19, 19, 1));
		assertTrue(model.addAbsorber("A2", 5, 5, 5, 5));
		
		// Adding where an absorber already exists
		assertFalse(model.addAbsorber("A3", 5, 5, 5, 5));
		
		// Adding an absorber where a gizmo already exists
		assertTrue(model.addGizmo("Square", "S", 2, 2));
		assertFalse(model.addAbsorber("A4", 0, 1, 10, 10));
		
		// Adding a null
		assertFalse(model.addAbsorber(null, 0, 0, 0, 0));
	}

}