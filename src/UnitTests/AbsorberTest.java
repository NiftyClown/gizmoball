package UnitTests;

import org.junit.Before;
import org.junit.Test;

import Model_Gizmos.Absorber;
import Model_Gizmos.Ball;

public class AbsorberTest {

	// Test variables
	Ball b1, b2, b3;
	Absorber a;

	@Before
	public void setUp() {
		// Create the balls
		b1 = new Ball("b1", 1, 2, 1.0, 2.0);
		b2 = new Ball("b2", 2, 3, 2.0, 3.0);
		b3 = new Ball("b3", 3, 4, 3.0, 4.0);

		// Create the Absorber
		a = new Absorber("a1", 0, 0, 4, 1);
	}

	@Test
	public void test() {
		// Absorb all the balls
		a.absorbBall(b1);
		a.absorbBall(b2);
		a.absorbBall(b3);
		a.absorbBall(b1); // Add it again, check coverage to ensure it returns
		a.absorbBall(null); // Null check

		a.action();
		a.action();
		a.action();
		a.action(); // Console print at this point
	}
}