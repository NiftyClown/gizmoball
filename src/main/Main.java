package main;

import javax.swing.UIManager;

import view.RunGui;
import view.SplashScreen;
import Model.GameLoop;
import Model.Model_I_View;

/**
 * Main class used to run the application
 */
public class Main {

	public static void main(String[] args) {
		try {
			// Use the platform look and feel
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			System.out.println("Look and Feel error in Main");
		}

		Model_I_View model = new GameLoop();

	    SplashScreen splash = new SplashScreen(3000);
	    splash.showSplash();
	    
		RunGui gui = new RunGui(model);
		gui.createAndShowGUI();

	}
}
