package flipper;

import java.util.ArrayList;

import physics.Circle;
import physics.LineSegment;
import physics.Vect;
import Model_Gizmos.AbstractGizmo;

public abstract class AbstractFlipper extends AbstractGizmo {

	protected double anchorX;
	protected double anchorY;
	protected double tipX;
	protected double tipY;
	protected final static double lengthOfPhysicsLine = 1.5;	//This assumes that L is used as a unit of measuring
	protected G2DPoint anchor;	//The centre of the anchor sides physics circle, which all points must be rotates around
	protected G2DPoint topOfFlipper;	//The very top of the Flipper at the anchor side.
	protected G2DPoint tip;	//The farthest most point that the flipper reaches
	private G2DPoint line1Anchor;	//The point at the anchor side of the left most physics line
	private G2DPoint line1Tip;	//The point at the tip side of the left most physics line
	private G2DPoint line2Anchor;	//The point at the anchor side of the right most physics line
	private G2DPoint line2Tip;	//The point at the tip side of the right most physics line
	private G2DPoint tipPhysicsCircle;	//The location of the centre point used to place the physics circle that will be at the tip of the flipper.
	protected ArrayList<G2DPoint> pointsToRotate;
	protected boolean active = false;
	protected final static double rotationSpeed  = 6 * Math.PI;	//Rads per second   
	protected String id;

	public AbstractFlipper(String id, int x, int y) {
		super(id, x, y);
		// Set the default colour to green
		super.setColour(0x00FF1A);
	}

	/**
	 * This will return the flippers current position in the form clearly visible in the return statement.
	 * @return
	 */
	public double[] getFlipper(){
		return new double[] {anchorX, anchorY, tipPhysicsCircle.getX(), tipPhysicsCircle.getY()};	
	}

	/**
	 * This will return the coodinates of the lines for use in building the physics and view outlines
	 * It will return it in the following format where 'anchor' references the points nearest the flippers
	 * anchor point and 'tip' references the points nearest the tip of the flipper
	 * [leftLineAnchorX, leftLineAnchorY, leftLineTipX, leftLineTipY,  rightLineAnchorX, rightLineAnchorY,
	 * rightLineTipX, rightLineTipY]
	 * @return
	 */
	protected void setActive(){
		active  = !active;
	}
	
	public void setCoordinates (int x, int y) {
		super.setCoordinates(x, y);
		anchorX  = x + 0.25 ;
		setPointsAndPhysics();
	}

	/**
	 * This allows all the points and physics objects to be set correctly after the Flippers have been created 
	 */
	protected void setPointsAndPhysics(){
		anchorY = super.getY()+0.25;
		tipX = anchorX;
		tipY = anchorY+lengthOfPhysicsLine+0.25;
		anchor = new G2DPoint(anchorX, anchorY);
		topOfFlipper = new G2DPoint(anchorX, super.getY());
		tip = new G2DPoint(tipX, tipY);
		line1Anchor = new G2DPoint(anchorX-0.25, anchorY);
		line1Tip = new G2DPoint(tipX-.025, tipY);
		line2Anchor = new G2DPoint(anchorX+0.25, anchorY);
		line2Tip = new G2DPoint(tipX+0.25, tipY);
		tipPhysicsCircle = new G2DPoint(anchorX, anchorY+lengthOfPhysicsLine);
		pointsToRotate = new ArrayList<G2DPoint>();
		pointsToRotate.add(line1Anchor);
		pointsToRotate.add(line1Tip);
		pointsToRotate.add(line2Anchor);
		pointsToRotate.add(line2Tip);
		pointsToRotate.add(tipPhysicsCircle);

		// Add the lines of this flipper
		super.getLines().add(new LineSegment(line1Anchor.getX(), line1Anchor.getY(), line1Tip.getX(), line1Tip.getY()));
		super.getLines().add(new LineSegment(line2Anchor.getX(), line2Anchor.getY(), line2Tip.getX(), line2Tip.getY()));

		// Add the circles to the corners of this flipper
		super.getCircles().add(new Circle(anchorX, anchorY, 0.25));
		super.getCircles().add(new Circle(tipPhysicsCircle.getX(), tipPhysicsCircle.getY(), 0.25));
	}

	@Override
	public String getType() {
		return "Flipper";
	}

	@Override
	public void action() {
		setActive();
	}

	public abstract boolean getActive();

	/** 
	 * This will rotate the flipper by the degrees/tick and is continually called until the max rotation angle has been reached.
	 * It also rotates all physics points associated with the flipper and recreates the physics objects each time this
	 * is called to keep them up to date with new coordinates.
	 * Protected abstract void rotateFlipper(double rad);
	 * @return
	 */
	protected void rotateFlipper(double rad){
		super.getLines().clear();
		super.getCircles().clear();
		// Add the lines of this flipper

		super.getLines().add(new LineSegment(line1Anchor.getX(), line1Anchor.getY(), line1Tip.getX(), line1Tip.getY()));
		super.getLines().add(new LineSegment(line2Anchor.getX(), line2Anchor.getY(), line2Tip.getX(), line2Tip.getY()));

		// Add the circles to the corners of this flipper
		super.getCircles().add(new Circle(anchorX, anchorY, 0.25));
		super.getCircles().add(new Circle(tipPhysicsCircle.getX(), tipPhysicsCircle.getY(), 0.25));
	}

	public abstract void tick(double secs);

	/**
	 * This is the same class as that which was given to us by the author for Graphics with the unnecessary components 
	 * removed.
	 * @author Mark Dunlop 
	 *
	 */
	protected class G2DPoint {

		private Matrix pt;//storage for a point

		public G2DPoint(double x, double y){
			pt = new Matrix(3,1);
			pt.set(0, 0, x);
			pt.set(1, 0, y);
			pt.set(2, 0, 1);
		}

		public double getX() {
			return pt.get(0, 0);
		}

		public double getY() {
			return pt.get(1, 0);
		}

		public void transform(Matrix transformation) {
			pt = transformation.multiply(pt);
		}

		public void translate(double dx, double dy) {
			double[][] oArray = { {1,0,dx}, {0,1,dy},{0,0,1}};//move to origin
			Matrix move = new Matrix(oArray);
			this.transform(move );
		}

		public void rotateAroundOrigin(double rad) {
			double[][] rotateArray = {	{ Math.cos(rad), -Math.sin(rad), 0},
					{ Math.sin(rad),  Math.cos(rad), 0},
					{ 0,0,1} }; //rotate 'rad' degrees
			Matrix rotate = new Matrix(rotateArray);
			transform(rotate);
		}

		public void rotateAroundPoint(G2DPoint anchor, double rad){

			double logiX = anchor.getX();
			double logiY = anchor.getY();

			//A = move to origin matrix
			this.translate(-logiX, -logiY);

			//B = Rotate by 'rad' degrees.
			this.rotateAroundOrigin(rad);

			//C = move back to point
			this.translate(logiX, logiY);

		}
	}

	/**
	 * This is the same class as that which was given to us by the author for Graphics with the unnecessary components 
	 * removed.
	 * @author Mark Dunlop 
	 *
	 */
	private class Matrix {

		private double[][] thematrix;
		private int rows, cols;

		public Matrix(double[][] matrixAsArray){
			this(matrixAsArray.length, matrixAsArray[0].length);
			this.thematrix = matrixAsArray;
		}

		public Matrix(int rows, int cols) {
			this.rows = rows;
			this.cols = cols;
			this.thematrix = new double[rows][cols];
		}

		public void set(int row, int col, double value) {
			thematrix[row][col]=value;
		}

		public double get(int row, int col) {
			return thematrix[row][col];
		}

		public Matrix multiply(Matrix other){
			if (this.cols!=other.rows) throw new IllegalArgumentException("Cannot multiply a "+this.shapeString()+
					" by a "+ other.shapeString()+" matrix");
			Matrix result = new Matrix(this.rows, other.cols);
			for (int r=0; r<this.rows; r++)
				for (int c=0; c<other.cols; c++){
					// set result[r][c] to dot product of this.r * other.c
					double dot = 0;
					for (int i=0; i<this.cols; i++)
						dot += this.thematrix[r][i] * other.thematrix[i][c];
					result.thematrix[r][c]=dot;
				}
			return result;
		}

		private String shapeString() {
			return rows+"x"+cols;
		}
		public String toString(){
			StringBuffer s = new StringBuffer();
			for (int r=0; r<rows; r++) {
				s.append("[ ");
				for (int c=0; c<cols; c++)
					s.append(String.format("%1$.2f", thematrix[r][c])+" ");
				s.append("]");
			}
			return "[ "+s+" ]";
		}

	}
	public Vect getAnchor() {
		return new Vect(anchorX, anchorY);
	}
}