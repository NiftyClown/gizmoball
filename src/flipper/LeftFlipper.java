package flipper;

public class LeftFlipper extends AbstractFlipper {

	private final double minAng = 0.5*Math.PI, maxAng = Math.PI;
	private double currAng = maxAng;

	public LeftFlipper(String id, int x, int y) {
		super(id, x, y);
		super.anchorX = x+0.25;
		super.setPointsAndPhysics();
		this.id = id;
	}

	/**
	 * This will rotate the flipper by the degrees/tick and is continually called until the max rotation angle has been reached.
	 */
	@Override
	protected void rotateFlipper(double rad){
		if(rad > 0) {
			rad = Math.min(rad, maxAng-currAng);
			currAng += rad;
		} 
		else if (rad < 0) {
			rad = Math.max(rad, -(currAng - minAng));
			currAng += rad;
		}
		if(rad != 0) {
			for(G2DPoint e : super.pointsToRotate){
				e.rotateAroundPoint(anchor, rad);
			}
			tip.rotateAroundPoint(topOfFlipper, rad);
		}
		super.rotateFlipper(rad);
	}

	@Override
	public void tick(double millies) {

		if(active && currAng > minAng){
			rotateFlipper(-rotationSpeed / (1000/millies));
		}
		else if (!active && currAng < maxAng){
			rotateFlipper(rotationSpeed / (1000/millies));
		}
	}

	@Override
	public String toString() {
		String temp = "LeftFlipper " + super.getID() + " " + Integer.toString(super.getX()) + " " + Integer.toString(super.getY()) + "\n";
		temp += "Colour " + this.getID() + " " + this.getColour() + "\n";
		return temp;
	}

	@Override
	public boolean getActive() {
		if(currAng<maxAng && currAng>minAng){
			return true;
		}
		return false;
	}

	@Override
	public String getType() {
		return "LeftFlipper";
	}
}