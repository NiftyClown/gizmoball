package Model_Gizmos;

import java.util.LinkedList;
import java.util.Queue;

import physics.Circle;
import physics.LineSegment;
import physics.Vect;

/**
 * Implementation of the Absorber for the Gizmoball system.
 * 
 * It will store a Queue of balls and launch the head of the
 * Queue whenever the trigger is pressed. (Automatically if triggers
 * are not present)
 * 
 */
public class Absorber extends AbstractGizmo {

	/**The Queue of balls present within this Absorber.*/
	private Queue<Ball> currentBalls;
	/**The width of the absorber.*/
	private int w;
	/**The height of the absorber.*/
	private int h;

	/**
	 * Constructor for the class
	 * 
	 * @param id The unique id of this absorber.
	 * @param x The x coordinate of this absorber.
	 * @param y The y coordinate of this absorber.
	 * @param w The width of this absorber.
	 * @param h The height of this absorber.
	 */
	public Absorber(String id, int x, int y, int w, int h) {
		super(id, x, y);
		this.w = w;
		this.h = h;

		// Instantiate the data structures
		currentBalls = new LinkedList<Ball>();

		// Create the physics
		instantiatePhysics(x, y);

		// Default colour of the Absorber
		super.setColour(0xFF00FF);
	}

	/**
	 * Creates the physics objects for this gizmo.
	 * @param x The x coordinate of the gizmo.
	 * @param y The y coordinate of the gizmo.
	 */
	private void instantiatePhysics(int x, int y) {
		// Add the lines of this Absorber
		super.getLines().add(new LineSegment(x, y, x + w, y) );
		super.getLines().add(new LineSegment(x, y, x, y + h) );
		super.getLines().add(new LineSegment(x, y + h, x + w, y + h) );
		super.getLines().add(new LineSegment(x + w, y, x + w, y + h) );

		// Add the zero radius circles to the corners of this Absorber
		super.getCircles().add(new Circle(x, y, 0));
		super.getCircles().add(new Circle(x, y + h, 0));
		super.getCircles().add(new Circle(x + w, y, 0));
		super.getCircles().add(new Circle(x + w, y + h, 0));
	}

	/**
	 * @return The width of the absorber.
	 */
	public int getWidth(){
		return w;
	}

	/**
	 * @return The height of the absorber.
	 */
	public int getHeight(){
		return h;
	}

	/**
	 * Takes in a Ball then puts it in the
	 * Queue for launching
	 * 
	 * @param b The ball being absorbed.
	 */
	public void absorbBall(Ball b) {
		// Is the ball already in or is it null?
		if (b == null)
			return; // Return

		// Add the ball to the Queue
		b.stop();
		b.setExactX(super.getX() + (w - 1) + 0.5);
		b.setExactY(super.getY() + (h - 1) + 0.5);
		currentBalls.offer(b);
	}

	/**
	 * Launches the Ball from the 
	 * front of the Queue in the Absorber.
	 * 
	 * @return Ball at the front of the Queue
	 * @throws EmptyQueueException
	 * 
	 */
	private void launchBall()  {
		// Remove the Ball at the front of the Queue
		if (currentBalls.isEmpty())
			return; // Do nothing
		else {
			Ball b = currentBalls.poll();
			b.start();
			// To solve the issue of the ball firing the absorber off the map we
			// base the launch direction on the Y-point of the abosorber
			if (super.getY() > 10) {
				b.setExactY(super.getY() - 1);
				b.setVelo(new Vect(0, -50 ));
			} else {
				b.setExactY(super.getY() + h + 1);
				b.setVelo(new Vect(0, 50));
			}
		}
	}

	@Override
	public void action() {
		// Launch the ball
		this.launchBall();

	}

	@Override
	public String getType() {
		return "Absorber";
	}

	@Override
	public String toString(){
		String temp = this.getType() + " " + super.getID() + " " + this.getX() + " " + this.getY() + " " + this.getWidth() + " " +
				this.getHeight() + "\n";
		temp+= "Colour "  + this.getID() + " " + this.getColour() + "\n";
		return temp;
	}

	@Override
	public void setCoordinates(int newX, int newY) {
		// Set the new coordinates
		super.setCoordinates(newX, newY);
		// Recreate the physics objects
		instantiatePhysics(newX, newY);
	}
}
