package Model_Gizmos;

import java.util.ArrayList;

import physics.LineSegment;

/**
 * Creates the Walls of the game field
 * so that the ball does not move out of play
 * 
 */
public class Walls {

	/**The x coordinate of the top left corner.*/
	private int xpos1;
	/**The y coordinate of the top left corner.*/
	private int ypos1;
	/**The y coordinate of the top right corner.*/
	private int ypos2;
	/**The x coordinate of the top right corner.*/
	private int xpos2;

	// Walls are the enclosing Rectangle - defined by top left corner and bottom right
	/**
	 * Constructor
	 * 
	 * @param x1 The x coordinate of the top left corner.
	 * @param y1 The y coordinate of the top left corner.
	 * @param x2 The x coordinate of the top right corner.
	 * @param y2 The y coordinate of the top right corner.
	 * @since Feb 13, 2014
	 */
	public Walls(int x1, int y1, int x2, int y2) {
		xpos1 = x1;
		ypos1 = y1;
		xpos2 = x2;
		ypos2 = y2;
	}

	/**
	 * @return LineSegments The lines associated with the wall.
	 * @since Feb 13, 2014
	 */
	public ArrayList<LineSegment> getLineSegments() {
		// Instantiate the list
		ArrayList<LineSegment> ls = new ArrayList<LineSegment>();

		// Create the LineSegments
		LineSegment l1 = new LineSegment(xpos1, ypos1, xpos2, ypos1);
		LineSegment l2 = new LineSegment(xpos1, ypos1, xpos1, ypos2);
		LineSegment l3 = new LineSegment(xpos2, ypos1, xpos2, ypos2);
		LineSegment l4 = new LineSegment(xpos1, ypos2, xpos2, ypos2);

		// Add them to the List
		ls.add(l1);
		ls.add(l2);
		ls.add(l3);
		ls.add(l4);

		return ls;
	}

}
