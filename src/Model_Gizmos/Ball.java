package Model_Gizmos;

import physics.Circle;
import physics.Vect;

/**
 * Implementation of the Ball.
 * 
 * Extends the {@link AbstractGizmo} for cohesion
 * among operations on the model.
 *
 */

public class Ball extends AbstractGizmo {

	/** The velocity of the ball.*/
	private Vect velocity;
	/** The x coordinate of the center of the ball.*/
	private double xpos;
	/** The y coordinate of the center of the ball.*/
	private double ypos;
	/** Whether or not the ball is stopped.*/
	private boolean stopped;
	/** The original velocity of the ball */
	private Vect originalVelo;

	/**
	 * Default constructor
	 * 
	 * @param id The id of the ball.
	 * @param x The x coordinate of the center of the ball.
	 * @param y The y coordinate of the center of the ball.
	 * @param xv The x vector of the ball.
	 * @param yv The y vector of the ball.
	 */
	public Ball(String id, double x, double y, double xv, double yv) {
		super(id, (int) x, (int) y);
		super.setColour(0x0000FF);

		// Centre coordinates
		xpos = x + 0.25; 
		ypos = y + 0.25;
		velocity = new Vect(xv, yv);
		originalVelo = velocity;
		stopped = false;
	}

	/**
	 * @return The current velocity of the ball.
	 */
	public Vect getVelo() {
		return velocity;
	}
	
	/**
	 * @return The original velocity of the ball
	 */
	public Vect getOriginalVelo() {
		return originalVelo;
	}

	/**
	 * @param v The new velocity to be used by the ball.
	 */
	public void setVelo(Vect v) {
		velocity = v;
	}

	/**
	 * @return c
	 * 		The physics Circle object associated with this Ball
	 */
	public Circle getCircle() {
		return new Circle(xpos + 0.25, ypos + 0.25, 0.25);
	}

	/**
	 * @return xpos The current value of x for the gizmo.
	 */
	public double getExactX() {
		return xpos;
	}

	/**
	 * @return ypos The current value of y for the gizmo.
	 */
	public double getExactY() {
		return ypos;
	}

	/**
	 * @param x The new value of x to be used.
	 */
	public void setExactX(double x) {
		xpos = x;
	}

	/**
	 * 
	 * @param y The new value of y to be used.
	 */
	public void setExactY(double y) {
		ypos = y;
	}

	/**
	 * Sets the state of the ball to stopped.
	 */
	public void stop() {
		stopped = true;
	}

	/**
	 * Sets the state of the ball to not stopped.
	 */
	public void start() {
		stopped = false;
	}

	/**
	 * @return True if the ball is stopped and false otherwise.
	 */
	public boolean stopped() {
		return stopped;
	}

	@Override
	public int hashCode() {
		return super.getID().hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ball other = (Ball) obj;
		if (super.getID() == null) {
			if (other.getID() != null)
				return false;
		} else if (!super.getID().equals(other.getID()))
			return false;
		return true;
	}

	@Override
	public String toString(){
		return "Ball " + super.getID() + " " + (double)super.getX() + " " + (double)super.getY() + " " + (double)Math.round(originalVelo.x()) + " " + (double)Math.round(originalVelo.y()) + "\n";
	}

	@Override
	public String getType() {
		return "Ball";
	}

	@Override
	public void setCoordinates(int newX, int newY) {
		super.setCoordinates(newX, newY);

		xpos = newX + 0.25;
		ypos = newY + 0.25;
	}
}
