package Model_Gizmos;

import physics.Circle;
import physics.LineSegment;

/**
 * A representation of the Square Gizmo
 * within the system.
 *
 */
public class Square extends AbstractGizmo {

	/**
	 * 
	 * @param id The unique id of the square.
	 * @param x The x coordinate of the square.
	 * @param y The y coordinate of the square.
	 */
	public Square(String id, int x, int y) {
		super(id, x, y);

		instantiatePhysics(x, y);
	}

	/**
	 * Creates the physics objects for this gizmo.
	 * @param x The x coordinate of the gizmo.
	 * @param y The y coordinate of the gizmo.
	 */
	private void instantiatePhysics(int x, int y) {
		// Add the lines of this square
		super.getLines().add(new LineSegment(x, y, x + 1, y) );
		super.getLines().add(new LineSegment(x, y, x, y + 1) );
		super.getLines().add(new LineSegment(x, y + 1, x + 1, y + 1) );
		super.getLines().add(new LineSegment(x + 1, y, x + 1, y + 1) );

		// Add the zero radius circles to the corners of this square
		super.getCircles().add(new Circle(x, y, 0));
		super.getCircles().add(new Circle(x, y + 1, 0));
		super.getCircles().add(new Circle(x + 1, y, 0));
		super.getCircles().add(new Circle(x + 1, y + 1, 0));
	}

	@Override
	public String getType() {
		return "Square";
	}

	@Override
	public void setCoordinates(int newX, int newY) {
		// Set the new coordinates
		super.setCoordinates(newX, newY);
		// Recreate the physics objects
		instantiatePhysics(newX, newY);
	}

}
