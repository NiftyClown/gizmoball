package Model_Gizmos;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import physics.Circle;
import physics.LineSegment;

/**
 * The abstract implementation of the Gizmo interface so that we can dictate
 * common functionality among the methods.
 * 
 * @author Barry
 * 
 */
public abstract class AbstractGizmo implements Gizmo_I {

	/** The x coordinate of the gizmo. */
	private int x;
	/** The y coordinate of the gizmo. */
	private int y;
	/** The id of the gizmo. */
	private final String gizmoID;
	/** The colour of the gizmo. */
	private int gizmoColour;

	/** The physics circles associated with the gizmo. */
	private Set<Circle> physCircles;
	/** The physics lines associated with the gizmo. */
	private Set<LineSegment> physLines;

	/**
	 * @param id
	 *            The id of the gizmo.
	 * @param newX
	 *            The x coordinate of the gizmo.
	 * @param newY
	 *            The y coordinate of the gizmo.
	 */
	public AbstractGizmo(String id, int newX, int newY) {
		gizmoID = id;
		x = newX;
		y = newY;

		// Default colour for all Gizmos
		gizmoColour = 0xFF0000;

		// Instantiate the sets
		physCircles = new HashSet<Circle>();
		physLines = new HashSet<LineSegment>();
	}

	@Override
	public void setX(int newX) {
			x = newX;
	}

	@Override
	public void setY(int newY) {
			y = newY;
	}

	@Override
	public int getX() {
		return x;
	}

	@Override
	public int getY() {
		return y;
	}

	@Override
	public void setCoordinates(int x, int y) {
		// Set the new coordinates
		if (x >= 0 && x <= 19 && y >= 0 && y <= 19) {
			setX(x);
			setY(y);

			// Clear the physics
			physLines.clear();
			physCircles.clear();
		}
	}

	@Override
	public String getID() {
		return gizmoID;
	}

	@Override
	public int getColour() {
		return gizmoColour;
	}

	@Override
	public void setColour(int c) {
		gizmoColour = c;
	}

	@Override
	public Set<Circle> getCircles() {
		return physCircles;
	}

	@Override
	public Set<LineSegment> getLines() {
		return physLines;
	}

	@Override
	public void rotate() {
		// Do nothing unless the Gizmo can rotate
	}

	@Override
	public void action() {
		// Generic action. Set a random colour
		gizmoColour = new Random().nextInt(16777216);
	}

	@Override
	public boolean equals(Object g) {
		if (!(g instanceof Gizmo_I))
			return false;
		Gizmo_I giz = (Gizmo_I) g;
		if (giz.getID().equals(getID()))
			return true;
		else
			return false;
	}

	@Override
	public abstract String getType();

	@Override
	public String toString() {
		String temp = this.getType() + " " + this.getID() + " " + this.getX()
				+ " " + this.getY() + "\n";
		temp += "Colour " + this.getID() + " " + this.getColour() + "\n";
		return temp;
	}

}
