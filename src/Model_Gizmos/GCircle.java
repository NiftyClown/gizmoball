package Model_Gizmos;

import physics.Circle;

/**
 * Representation of a Circle Gizmo
 * within the Gizmoball system.
 *
 */
public class GCircle extends AbstractGizmo {

	/**
	 * The constructor for the the class.
	 * Takes in 3 parameters, the coordinates
	 * and the value of L.
	 * 
	 * @param id The id of the circle 
	 * @param x The x coordinate
	 * @param y The y coordinate 
	 */
	public GCircle (String id, int x, int y) {
		super(id, x, y);
		// Default colour of Circles
		super.setColour(0x009999);

		// Add in the circle to the set which represents this circle object.
		instantiatePhysics(x, y);
	}

	/**
	 * Creates the physics objects for this gizmo.
	 * @param x The x coordinate of the gizmo.
	 * @param y The y coordinate of the gizmo.
	 */
	private void instantiatePhysics(int x, int y) {
		// Add in the circle to the set which represents this circle object.
		super.getCircles().add(new Circle(x + 0.5, y + 0.5, 0.5));
	}

	@Override
	public String getType() {
		return "Circle";
	}

	@Override
	public void setCoordinates(int newX, int newY) {
		// Set the new coordinates
		super.setCoordinates(newX, newY);
		// Recreate the physics objects
		instantiatePhysics(newX, newY);
	}

}
