package Model_Gizmos;

import java.util.Set;

import physics.Circle;
import physics.LineSegment;

/**
 * The interface for all Gizmos within the
 * system.
 * 
 * Dictates common methods to be implemented by all Gizmos
 * @author Barry
 *
 */

public interface Gizmo_I {

	/**
	 * @param newX
	 * 		The new x coordinate
	 */
	public void setX(int newX);

	/**
	 * @param newY
	 * 		The new y coordinate
	 */
	public void setY(int newY);

	/**
	 * @return x 
	 * 		The x coordinate of this gizmo.
	 */
	public int getX();

	/**
	 * @return y 
	 * 		The y coordinate of this gizmo.
	 */
	public int getY();

	/**
	 * @return id 
	 * 		The unique id of this gizmo or an empty string if the gizmo isn't on the board.
	 */
	public String getID();

	/**
	 * @return The type of this gizmo.
	 */
	public String getType();

	/**
	 * @return The colour of this gizmo as a hex value
	 */
	public int getColour();

	/**
	 * Set the colour of this gizmo
	 * @param newC
	 */
	public void setColour(int newC);

	/**
	 * @param x The new x coordinate of this gizmo.
	 * @param y The new y coordinate of this gizmo.
	 */
	public void setCoordinates(int newX, int newY);

	/**
	 * @return The physics circles associated with the gizmo.
	 */
	public Set<Circle> getCircles();

	/**
	 * @return The physics lines associated with the gizmo.
	 */
	public Set<LineSegment> getLines();

	/**
	 * Rotates the Gizmo by 90 degrees.
	 */
	public void rotate();

	/**
	 * Performs the Gizmos specified additional functionality.
	 * 
	 */
	public void action();

}
