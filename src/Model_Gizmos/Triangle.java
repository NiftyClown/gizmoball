package Model_Gizmos;

import java.util.HashSet;
import java.util.Set;

import physics.Angle;
import physics.Circle;
import physics.Geometry;
import physics.LineSegment;
import physics.Vect;

/**
 * Implementation of a Triangle Gizmo in the Gizmoball system
 * 
 * @author Barry
 * 
 */
public class Triangle extends AbstractGizmo {

	/** The current orientation of this Triangle */
	private int ori;

	/**
	 * @param id
	 *            The unique id of the triangle.
	 * @param x
	 *            The x coordinate of the triangle.
	 * @param y
	 *            The y coordinate of the triangle.
	 */
	public Triangle(String id, int x, int y) {
		super(id, x, y);

		// Set the default colour
		super.setColour(0xFFBF00);

		// Create the physics
		instantiatePhysics(x, y);

		ori = 0;
	}

	/**
	 * Creates the physics objects for this gizmo.
	 * 
	 * @param x
	 *            The x coordinate of the gizmo.
	 * @param y
	 *            The y coordinate of the gizmo.
	 */
	private void instantiatePhysics(int x, int y) {
		// Add the lines of this triangle
		super.getLines().add(new LineSegment(x, y, x + 1, y + 1));
		super.getLines().add(new LineSegment(x + 1, y + 1, x, y + 1));
		super.getLines().add(new LineSegment(x, y + 1, x, y));

		// Add the zero radius circles to the corners of this triangle
		super.getCircles().add(new Circle(x, y, 0));
		super.getCircles().add(new Circle(x + 1, y + 1, 0));
		super.getCircles().add(new Circle(x, y + 1, 0));
	}

	@Override
	public void rotate() {
		// The point we're rotating around
		Vect cent = new Vect(super.getX() + 0.5, super.getY() + 0.5);
		Set<LineSegment> newLines = new HashSet<LineSegment>();
		Set<Circle> newCircles = new HashSet<Circle>();

		// Rotate
		for (LineSegment l : super.getLines()) {
			l = Geometry.rotateAround(l, cent, Angle.DEG_90);
			newLines.add(l);
		}

		for (Circle c : super.getCircles()) {
			c = Geometry.rotateAround(c, cent, Angle.DEG_90);
			newCircles.add(c);
		}

		// Clear and reset
		super.getLines().clear();
		super.getCircles().clear();

		super.getLines().addAll(newLines);
		super.getCircles().addAll(newCircles);
		ori = (ori + 1) % 4;

	}

	@Override
	public String getType() {
		return "Triangle";
	}

	@Override
	public String toString() {
		// we need to include dem rotates
		String temp = super.toString();
		for (int i = 0; i < ori; i++)
			temp += "Rotate " + this.getID() + " \n";
		return temp;
	}

	@Override
	public void setCoordinates(int x, int y) {
		// Set the new coordinates
		super.setCoordinates(x, y);

		// Recreate the physics objects
		instantiatePhysics(x, y);

		// Rotate
		int oriVal = ori;
		for (int i = 0; i < oriVal; i++) {
			ori--;
			rotate();
		}

	}

}
