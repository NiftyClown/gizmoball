package Model;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Set;

import physics.Circle;
import physics.Geometry;
import physics.LineSegment;
import physics.Vect;
import Model_Gizmos.Absorber;
import Model_Gizmos.Ball;
import Model_Gizmos.GCircle;
import Model_Gizmos.Gizmo_I;
import Model_Gizmos.Square;
import Model_Gizmos.Triangle;
import Model_Gizmos.Walls;
import flipper.AbstractFlipper;
import flipper.LeftFlipper;
import flipper.RightFlipper;

/**
 * Implementation of the Model
 * 
 */
public class GameLoop extends Observable implements Model_I, Model_I_View, Model_I_Controller {

	/** The set of all Gizmos and Balls within this model. */
	private Set<Gizmo_I> gizmos;
	/** Maps gizmos to the keys that trigger them when released. */
	private Map<Integer, List<Gizmo_I>> keyUpConnects;
	/** Maps gizmos to the keys that trigger them when pressed. */
	private Map<Integer, List<Gizmo_I>> keyDownConnects;
	/** The ball in play. */
	private Set<Ball> balls;

	/** Maps gizmos to gizmos connected to it.. */
	private Map<Gizmo_I, List<Gizmo_I>> connects;

	/** The walls of the board. */
	private Walls gws;

	/** The height of the board */
	private int height;
	/** The width of the board */
	private int width;
	/** The value of L */
	private static final int L = 24;

	/** The number of gizmos in the board. */
	int gizCount = 0;

	/** The intensity of gravity. */
	private double gravity;
	/** The intensity of friction. */
	private double mu1, mu2;

	/** Representation of the board. */
	private Grid board;
	/** Background colour of the game */
	private int colour;

	/**
	 * Constructor for the Abstract gizmo
	 * 
	 */
	public GameLoop() {
		// Instantiate the sets
		gizmos = new HashSet<Gizmo_I>();
		connects = new HashMap<Gizmo_I, List<Gizmo_I>>();
		keyUpConnects = new HashMap<Integer, List<Gizmo_I>>();
		keyDownConnects = new HashMap<Integer, List<Gizmo_I>>();
		balls = new HashSet<Ball>();

		// Create the walls...
		gws = new Walls(0, 0, 20, 20);

		// Set the height, width and create the board
		height = width = 20;
		board = new Grid(height, width);

		// Set gravity and friction
		gravity = 25;
		mu1 = 0.025;
		mu2 = 0.025;

		// Default colour values
		colour = 0x000000;
	}

	@Override
	public Set<Ball> getBalls() {
		return balls;
	}

	@Override
	public Set<Gizmo_I> getGizmos() {
		return gizmos;
	}

	@Override
	public Set<Integer> getKeyUpConnects() {
		return keyUpConnects.keySet();
	}

	@Override
	public Set<Integer> getKeyDownConnects() {
		return keyDownConnects.keySet();
	}

	@Override
	public int getBackgroundColour() {
		return colour;
	}

	/**
	 * @return L The current value of L in the game.
	 */
	public static int getL() {
		return L;
	}

	@Override
	public String gizmoAt(int x, int y) {
		String base = board.get(x, y);
		return gizmoAt(base).getID();
	}

	@Override
	public Gizmo_I gizmoAt(String id) {
		// Find what we need
		for (Gizmo_I g : gizmos)
			if (g.getID().equals(id))
				return g;

		// Must be a ball
		for (Ball b : balls)
			if (b.getID().equals(id))
				return b;

		return null;
	}

	@Override
	public void setGravity(float g) {
		gravity = g;
	}

	@Override
	public void setFriction(float x, float y) {
		mu1 = x;
		mu2 = y;
	}

	@Override
	public void setGizmoColour(int x, int y, int rgb) {
		// Get the Gizmo at these coordinates
		assert gizmoAt(x, y) != null : "Error: Gizmo is null";
		Gizmo_I g = gizmoAt(gizmoAt(x, y));
		g.setColour(rgb);

		// Notify observers
		this.setChanged();
		this.notifyObservers();
	}
	
	@Override
	public void setGizmoColour(String id, int rgb) {
		// Get the Gizmo at these coordinates
		assert gizmoAt(id) != null : "Error: Gizmo is null";
		Gizmo_I g = gizmoAt(id);
		g.setColour(rgb);

		// Notify observers
		this.setChanged();
		this.notifyObservers();
	}

	@Override
	public void setBackgroundColour(int rgb) {
		colour = rgb;

		// Notify observers
		this.setChanged();
		this.notifyObservers();
	}

	@Override
	public void moveBall() {
		// The maximum amount of time a Ball moves for
		double moveTime = 0.005;

		// Go through all of the balls
		for (Ball ball : balls) {

			if (ball != null && !ball.stopped()) {
				// Apply friction and gravity
				ball.setVelo(getEffect(ball.getVelo(), moveTime));

				// Time to collide
				CollisionDetails cd = timeUntilCollision(ball);
				double tuc = cd.getTuc();

				if (tuc > moveTime) {
					// No collision ...
					ball = movelBallForTime(ball, moveTime);
				} else {
					moveTime = tuc;

					if (moveTime == 0.0) {
						moveTime = 0.01; // Stops the game from stopping 
					}

					// We've got a collision in tuc
					ball = movelBallForTime(ball, tuc);
					// Post collision velocity ...
					ball.setVelo(cd.getVelo());
					// The gizmo
					Gizmo_I g = cd.getGiz();

					// Events
					if (g instanceof Absorber) {
						((Absorber) g).absorbBall(ball);
					}
					
					if (connects.containsKey(g) && g != null) {
						for (Gizmo_I ck : connects.get(g)) {
							if (ck == null)
								; // Do nothing
							else
								ck.action();
						}
					}

				}
			}

		}

		// Go through all the flippers and move them for the specified time
		for (Gizmo_I e : gizmos) {
			if (e instanceof AbstractFlipper) {
				((AbstractFlipper) e).tick(moveTime * 1000);
			}
		}

		// Notify observers ... redraw updated view
		this.setChanged();
		this.notifyObservers();
	}

	@Override
	public boolean addGizmo(String type, String id, int x, int y) {
		// Variable
		Gizmo_I g;
		
		// Null check
		if (type == null || id == null)
			return false;

		// Check to see if we can add it
		if (checkBoard(type, x, y))
			return false;

		// Check if the id is empty
		if (id.isEmpty())
			id = type.concat(Integer.toString(gizCount));

		// Figure out what the type is...
		switch (type) {

		case ("Square"):
			g = new Square(id, x, y);
		break;

		case ("Circle"):
			g = new GCircle(id, x, y);
		break;

		case ("Triangle"):
			g = new Triangle(id, x, y);
		break;

		case ("RightFlipper"):
			g = new RightFlipper(id, x, y);
		break;

		case ("LeftFlipper"):
			g = new LeftFlipper(id, x, y);
		break;

		default:
			System.out.println("Invalid Gizmo type.");
			return false;
		}

		// Assertion
		assert g != null : "Error: The Gizmo we have is null";

		// Add it to the set and return
		gizmos.add(g);
		addToBoard(g);

		// Increment
		gizCount++;

		// Notify observers
		this.setChanged();
		this.notifyObservers();

		return true;
	}

	@Override
	public boolean connect(String first, String second) {
		
		// Null check
		if (first == null || second == null)
			return false;
		
		// Variables
		Gizmo_I g1, g2;

		// Get them
		g1 = gizmoAt(first);
		g2 = gizmoAt(second);

		// Add the connection
		if (connects.containsKey(g1)) {
			for (Gizmo_I g : connects.get(g1)) {
				if (g.equals(g2)) 
					return false; // Do nothing
				else  {
					connects.get(g1).add(g2);
					return true;
				}
			}
		} else {
			connects.put(g1, new ArrayList<Gizmo_I>());
			connects.get(g1).add(g2);
			return true;
		}
		
		return false;
	}

	@Override
	public boolean keyConnect(int keyId, String dir, String name) {
		if (gizmoAt(name) == null || dir == null || name == null) {
			return false;
		} else if(dir.equals("up")) {
			if (!keyUpConnects.containsKey(keyId)) {
				ArrayList<Gizmo_I> temp = new ArrayList<Gizmo_I>();
				temp.add(gizmoAt(name));
				keyUpConnects.put(keyId, temp);
			} else {
				List<Gizmo_I> temp = keyUpConnects.get(keyId);
				temp.add(gizmoAt(name));
			}
			
			return true;
		} else if (dir.equals("down")) {
			if (!keyDownConnects.containsKey(keyId)) {
				ArrayList<Gizmo_I> temp = new ArrayList<Gizmo_I>();
				temp.add(gizmoAt(name));
				keyDownConnects.put(keyId, temp);
			} else {
				List<Gizmo_I> temp = keyDownConnects.get(keyId);
				temp.add(gizmoAt(name));
			}
			return true;
		}
		
		return false;
	}

	@Override
	public boolean move(String name, int x, int y) {
		// Null check
		if (name == null)
			return false;
		
		// The Gizmo we're moving
		Gizmo_I g = gizmoAt(name);

		// Check if we can add there
		if (checkBoard(g.getID(), x, y))
			return false;

		// Remove it's original spot
		deleteFromBoard(g);

		// Place it in the new spot
		g.setCoordinates(x, y);
		addToBoard(g);

		// Notify observers
		this.setChanged();
		this.notifyObservers();

		return true;
	}

	@Override
	public void delete(String name) {
		// Null check
		if (name == null)
			return; // Do nothing
		
		// The Gizmo we're removing
		Gizmo_I g = gizmoAt(name);

		// Remove it from the board
		deleteFromBoard(g);
		gizmos.remove(g);
		if (g.getType().equals("Ball"))
			balls.remove(g);

		// Notify observers
		this.setChanged();
		this.notifyObservers();
	}

	@Override
	public boolean addBall(String id, double x, double y, double xv, double yv) {

		// Check to see if we can place it here
		if (board.check((int) x, (int) y) || balls.size() == 1 || id == null) {
			return false;
		} else {
			// We can add
			// Check if the id is empty
			if (id.isEmpty())
				id = "B".concat(Integer.toString(gizCount));

			Ball b = new Ball(id, (int) x, (int) y, xv, yv);

			gizCount++;
			board.add(id, (int) x, (int) y);
			balls.add(b);
			gizmos.add(b);
		}

		// Notify observers
		this.setChanged();
		this.notifyObservers();
		return true;
	}

	@Override
	public boolean addAbsorber(String id, int x, int y, int w, int h) {
		
		// Null check
		if (id == null)
			return false;

		// Checks
		for (int i = 0; i < w; i++)
			for (int j = 0; j < h; j++)
				if (board.check(x + i, y + j))
					return false; // There is a Gizmo here

		if (id.isEmpty())
			id = "A".concat(Integer.toString(gizCount));

		// Increment
		gizCount++;

		// Create the Gizmo
		Gizmo_I a = new Absorber(id, x, y, w, h);

		// Add the Absorber in
		addToBoard(a);
		gizmos.add(a);

		// Notify observers
		this.setChanged();
		this.notifyObservers();
		return true;
	}

	@Override
	public boolean writeFile(File file) {
		 return Writer.writeFile(file, balls, gizmos, connects, keyUpConnects,
		 keyDownConnects, gravity, mu1, mu2);
	}

	@Override
	public void clear() {
		gizmos.clear();
		keyDownConnects.clear();
		keyUpConnects.clear();
		board.clear();
		balls.clear();
		gizCount = 0;
		gravity = 25;
		mu1 = 0.025;
		mu2 = 0.025;

		// Notify observers
		this.setChanged();
		this.notifyObservers();
	}

	@Override
	public void reset() {
		// Remove the ball from all absorbers
		for (Gizmo_I g : gizmos) {
			if (g.getType().equals("Absorber"))
				g.action();
		}

		// Set the position of the balls back to normal.
		for (Ball b : balls) {
			b.setExactX(b.getX() + 0.25);
			b.setExactY(b.getY() + 0.25);

			// Reinstate the default velocity
			b.setVelo(b.getOriginalVelo());
			b.start();
		}

		// Notify observers
		this.setChanged();
		this.notifyObservers();
	}

	@Override
	public void action(int keyCode, String dir) {
		if (dir.equals("up")) {
			for (Gizmo_I giz : keyUpConnects.get(keyCode))
				giz.action();
		} else {
			for (Gizmo_I giz : keyDownConnects.get(keyCode))
				giz.action();
		}

		// Notify observers
		this.setChanged();
		this.notifyObservers();
	}

	@Override
	public void rotate(String name) {
		for (Gizmo_I g : gizmos) {
			if (g.getID().equals(name))
				g.rotate();
		}
		this.setChanged();
		this.notifyObservers();

	}

	/**
	 * Returns a Vector with friction and friction applied to it
	 * 
	 * @param v
	 *            The original Vector
	 * @param time
	 *            The time it is being applied for
	 * @return newV The Vector with gravity and friction applied
	 */
	private Vect getEffect(Vect v, double time) {

		Vect grav = new Vect(0, gravity * time);
		v = v.plus(grav);
		v = v.times(1 - mu1 * time - mu2 * Math.abs(v.length()) * time);

		return v;
	}

	/**
	 * Moves the ball for the specified time
	 * 
	 * @param ball
	 *            ball to be moved.
	 * @param time
	 *            The time that the ball's to be moved for.
	 * @return The updated Ball.
	 * 
	 */
	private Ball movelBallForTime(Ball ball, double time) {

		// Assertion
		assert ball != null : "Error: The ball we're trying to move is null!";

		double newX = 0.0;
		double newY = 0.0;
		double xVel = ball.getVelo().x();
		double yVel = ball.getVelo().y();
		newX = ball.getExactX() + (xVel * time);
		newY = ball.getExactY() + (yVel * time);
		ball.setExactX(newX);
		ball.setExactY(newY);

		return ball;

	}

	/**
	 * @return CollisionDetails The details of any collision which may occur
	 *         when the ball is moving.
	 */
	private CollisionDetails timeUntilCollision(Ball ball) {

		// Find Time Until Collision and also, if there is a collision, the new
		// speed vector.
		// Create a physics.Circle from Ball
		Circle ballCircle = ball.getCircle();
		Vect ballVelocity = ball.getVelo();
		Vect newVelo = new Vect(0, 0);

		// Now find shortest time to hit a vertical line or a wall line
		double shortestTime = Double.MAX_VALUE;
		double time = 0.0;

		// The Gizmo_I which we will be colliding with
		Gizmo_I closestGiz = null;

		// Time to collide with 4 walls
		ArrayList<LineSegment> lss = gws.getLineSegments();
		for (LineSegment line : lss) {
			time = Geometry.timeUntilWallCollision(line, ballCircle,
					ballVelocity);
			if (time < shortestTime) {
				shortestTime = time;
				newVelo = Geometry.reflectWall(line, ballVelocity, 1.0);
			}
		}

		// Time to collide
		for (Gizmo_I g : gizmos) {

			if (!(g instanceof Ball)) {

				// Reflect off the lines of the Gizmo
				for (LineSegment ls : g.getLines()) {
					time = Geometry.timeUntilWallCollision(ls, ballCircle,
							ballVelocity);

					if (time < shortestTime) {
						// Assign new values
						shortestTime = time;
						closestGiz = g;

						// Are we working with a flipper?
						if (g instanceof AbstractFlipper
								&& ((AbstractFlipper) g).getActive()) {

							// Reflect a rotating wall
							time = Geometry
									.timeUntilRotatingWallCollision(ls,
											((AbstractFlipper) g).getAnchor(),
											6 * Math.PI, ball.getCircle(),
											ballVelocity);

							if (time < shortestTime) {
								shortestTime = time;
							}

							newVelo = Geometry.reflectRotatingWall(ls,
									((AbstractFlipper) g).getAnchor(),
									6 * Math.PI, ball.getCircle(),
									ball.getVelo(), 0.95);

						} else {
							newVelo = Geometry.reflectWall(ls, ball.getVelo(),
									1.0);
						}
					}
				}

				// Reflect off the Circles of the Gizmo
				for (Circle c : g.getCircles()) {
					time = Geometry.timeUntilCircleCollision(c, ballCircle,
							ballVelocity);

					if (time < shortestTime) {
						// Assign values
						shortestTime = time;
						closestGiz = g;

						// Are we working with a flipper
						if (g instanceof AbstractFlipper
								&& ((AbstractFlipper) g).getActive()) {

							time = Geometry.timeUntilRotatingCircleCollision(c,
									((AbstractFlipper) g).getAnchor(),
									6 * Math.PI, ball.getCircle(),
									ball.getVelo());

							if (time < shortestTime) {
								shortestTime = time;
							}

							newVelo = Geometry.reflectRotatingCircle(c,
									((AbstractFlipper) g).getAnchor(),
									6 * Math.PI, ball.getCircle(),
									ball.getVelo());

						} else {
							newVelo = Geometry.reflectCircle(c.getCenter(),
									ball.getCircle().getCenter(),
									ball.getVelo());

						}
					}
				}
			}
		}

		return new CollisionDetails(shortestTime, newVelo, closestGiz);
	}

	/**
	 * Deletes the gizmo from the board.
	 * @param g The gizmo to be deleted.
	 */
	private void deleteFromBoard(Gizmo_I g) {
		// Coordinates
		int x = g.getX();
		int y = g.getY();

		if (g.getType().equals("Absorber")) {
			for (int i = 0; i < ((Absorber) g).getWidth(); i++)
				for (int j = 0; j < ((Absorber) g).getHeight(); j++)
					board.delete(x + i, y + j);
		} else if (g.getType().equals("LeftFlipper")) {
			board.delete(x, y);
			board.delete(x + 1, y);
			board.delete(x, y + 1);
			board.delete(x + 1, y + 1);
		} else if (g.getType().equals("RightFlipper")) {
			;
			board.delete(x, y);
			board.delete(x - 1, y);
			board.delete(x, y + 1);
			board.delete(x - 1, y + 1);
		} else {
			board.delete(x, y);
		}
	}

	/**
	 * Adds a gizmo to the board.
	 * 
	 * @param g
	 *            The gizmo to be added
	 */
	private void addToBoard(Gizmo_I g) {
		// Coordinates
		int x = g.getX();
		int y = g.getY();

		if (g.getType().equals("Absorber")) {
			for (int i = 0; i < ((Absorber) g).getWidth(); i++)
				for (int j = 0; j < ((Absorber) g).getHeight(); j++)
					board.add(g.getID(), x + i, y + j);
		} else if (g.getType().equals("LeftFlipper")) {
			board.add(g.getID(), x, y);
			board.add(g.getID(), x + 1, y);
			board.add(g.getID(), x, y + 1);
			board.add(g.getID(), x + 1, y + 1);
		} else if (g.getType().equals("RightFlipper")) {
			;
			board.add(g.getID(), x, y);
			board.add(g.getID(), x - 1, y);
			board.add(g.getID(), x, y + 1);
			board.add(g.getID(), x - 1, y + 1);
		} else {
			board.add(g.getID(), x, y);
		}
	}

	/**
	 * Checks the board for a gizmo at coordinates.
	 * 
	 * @param type
	 *            The type of the Gizmo
	 * @param x
	 *            X coordinate
	 * @param y
	 *            Y coordinate
	 * @return result True if there is a gizmo at the x,y and false otherwise.
	 */
	private boolean checkBoard(String type, int x, int y) {
		if (type.equals("LeftFlipper")) {
			if (board.check(x, y) || board.check(x + 1, y)
					|| board.check(x, y + 1) || board.check(x + 1, y + 1))
				return true;
		} else if (type.equals("RightFlipper")) {
			;
			if (board.check(x, y) || board.check(x - 1, y)
					|| board.check(x, y + 1) || board.check(x - 1, y + 1))
				return true;
		} else {
			return board.check(x, y);
		}

		return false;
	}

}

