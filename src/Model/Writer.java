package Model;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import Model_Gizmos.Ball;
import Model_Gizmos.Gizmo_I;

/**
 * Writes the state of the game to a file
 * 
 */
public class Writer {
	public static boolean writeFile(File file, Set<Ball> balls,
			Set<Gizmo_I> gizmos, Map<Gizmo_I, List<Gizmo_I>> connects,
			Map<Integer, List<Gizmo_I>> keyUpConnects,
			Map<Integer, List<Gizmo_I>> keyDownConnects, double gravity,
			double mu1, double mu2) {

		FileWriter fw;
		try {
			fw = new FileWriter(file + ".txt");
			BufferedWriter bw = new BufferedWriter(fw);

			for (Gizmo_I gizmo : gizmos) {
				bw.write(gizmo.toString());
			}

			for (Entry<Gizmo_I, List<Gizmo_I>> e : connects.entrySet()) {
				for (Gizmo_I g : e.getValue())
					bw.write("Connect " + e.getKey().getID() + " " + g.getID()
							+ "\n");
			}

			for (Integer i : keyUpConnects.keySet()) {
				for (Gizmo_I g : keyUpConnects.get(i)) {
					bw.write("KeyConnect key " + i + " up "
							+ g.getID() + "\n");
				}
			}
			
			for (Integer i : keyDownConnects.keySet()) {
				for (Gizmo_I g : keyDownConnects.get(i)) {
					bw.write("KeyConnect key " + i + " down "
							+ g.getID() + "\n");
				}
			}

			bw.write("Gravity " + gravity + "\n");
			bw.write("Friction " + mu1 + "  " + mu2 + "\n");
			bw.close();
			fw.close();
			return true;
		} catch (IOException e) {
			System.out.println(e.getMessage());
			return false;
		}
	}

}
