package Model;

import java.util.Set;

import Model_Gizmos.Ball;
import Model_Gizmos.Gizmo_I;

/**
 * The interface which the View interacts with
 *
 */

public interface Model_I_View {

	/**
	 * Returns the set of all active
	 * ball objects currently in play 
	 * within the Model_I
	 * 
	 * @return The active Ball.
	 */
	public Set<Ball> getBalls();

	/**
	 * @return  The gizmos that are active within the board.
	 */
	public Set<Gizmo_I> getGizmos();

	/**
	 * @return Integer representation of the colour.
	 */
	public int getBackgroundColour();

}
