package Model;

import java.io.File;
import java.util.Set;

import Model_Gizmos.Ball;
import Model_Gizmos.Gizmo_I;

/**
 * Implementation of the Model interface
 * 
 */

public interface Model_I {

	/**
	 * Returns the set of all active
	 * ball objects currently in play 
	 * within the Model_I
	 * 
	 * @return The active Ball.
	 */
	public Set<Ball>getBalls();

	/**
	 * Move the ball for one tick.
	 */
	public void moveBall();

	/**
	 * @return Set of Gizmo_I The gizmos that are active within the board.
	 */
	public Set<Gizmo_I> getGizmos();

	/**
	 * @return Integer representing the colour of the board.
	 */
	public int getBackgroundColour();

	/**
	 * 
	 * @param type The type of the gizmo to be added.
	 * @param id The unique id of the gizmo to be added.
	 * @param x The x coordinate of the gizmo to be added.
	 * @param y The y coordinate of the gizmo to be added.
	 * @return True if the gizmo is added and false otherwise.
	 */
	public boolean addGizmo(String type, String id, int x, int y);

	/**
	 * @param gravity The new value of gravity to be used.
	 */
	public void setGravity(float gravity);

	/**
	 * @param x The new value of mu1 to be used.
	 * @param y The new value of mu2 to be used.
	 */
	public void setFriction(float x, float y);

	/**
	 * @param x
	 * 		The x coordinate of the Gizmo
	 * @param y
	 * 		The y coordinate of the Gizmo
	 * @param rgb
	 * 		The RGB value of the new colour
	 */
	public void setGizmoColour(int x, int y, int rgb);
	
	/**
	 * @param gizmo
	 * 		The ID of the Gizmo we're changing the colour of
	 * @param rgb
	 * 		The RGB value of the new colour
	 */
	public void setGizmoColour(String gizmo, int rgb);

	/**
	 * @param rgb
	 * 		The new background colour
	 */
	public void setBackgroundColour(int rgb);

	/**
	 * Connects two Gizmos by their IDs
	 * 
	 * @param first The unique name of the first gizmo to be connected.
	 * @param second The unique name of the second gizmo to be connected.
	 * @return result
	 */
	public boolean connect(String first, String second);

	/**
	 * Adds a key connection to a given
	 * Gizmo
	 * 
	 * @param keyId The key id to connect to the gizmo.
	 * @param name The unique name of the gizmo to connect to.
	 * @return result
	 */
	public boolean keyConnect(int keyId, String dir, String name);

	/**
	 * Moves a gizmo from one position on the board to another.
	 * 
	 * @param name The unique name of the gizmo to be moved.
	 * @param x The desired x coordinate for the gizmo.
	 * @param y The desired y coordinate for the gizmo.
	 * @return True if the spot is free and false otherwise.
	 */
	public boolean move(String name, int x, int y);

	/**
	 * @param name The name of the gizmo to be deleted.
	 */
	public void delete(String name);

	/**
	 * @param name The name of the gizmo to be rotated.
	 */
	public void rotate(String name);

	/**
	 * Adds a Ball to the system
	 * 
	 * @param name The unique name of the ball.
	 * @param x The x coordinate of the ball.
	 * @param y The y coordinate of the ball.
	 * @param xv The x vector of the ball.
	 * @param yv The y vector of the ball.
	 * @return 
	 */
	public boolean addBall(String name, double x, double y, double xv, double yv);

	/**
	 * Creates a new absorber in the model.
	 * 
	 * @param id The unique ID of the absorber.
	 * @param x The x coordinate of the absorber.
	 * @param y The y coordinate of the absorber.
	 * @param w The width of the absorber.
	 * @param h The height of the absorber.
	 */
	public boolean addAbsorber(String id, int x, int y, int w, int h);

	/**
	 * Writes the contents of the game board to file.
	 * 
	 * @param name The file to be written to.
	 * @return True if the write is successful and false otherwise.
	 */
	public boolean writeFile(File name);

	/**
	 * @param x The x coordinate to check for a gizmo.
	 * @param y The y coordinate to check for a gizmo.
	 * @return The gizmo at the given coordinates.
	 */
	public String gizmoAt(int x, int y);

	/**
	 * Fetches a Gizmo according to its ID
	 * 
	 * @param id
	 *            The id to be found.
	 * @return Gizmo The gizmo we're looking for or null if it doesn't exist.
	 */
	public Gizmo_I gizmoAt(String id);

	/**
	 * Clears the game board allowing a new one to be created.
	 */
	public void clear();

	/**
	 * Resets the state
	 */
	public void reset();

	/**
	 * Triggers the action associated with this key
	 * @param keyCode
	 * 		The key code.
	 * @param dir
	 * 		The direction
	 */
	public void action(int keyCode, String dir);

	/**
	 * @return The set of up key connects.
	 */
	public Set<Integer> getKeyUpConnects();

	/**
	 * @return The set of up key connects.
	 */
	public Set<Integer> getKeyDownConnects();
}