package Model;

/**
 * Representation of the game board
 *
 */

public class Grid {

	/** Our grid representation */
	private String[][] board;

	/**
	 * Constructor for our class
	 * 
	 * @param h
	 * 		The height of our grid
	 * @param w
	 * 		The width of our grid
	 */
	public Grid(int h, int w) {
		// Create the game board
		board = new String[h][w];

		// Fill it with nulls
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[i].length; j++) {
				board[i][j] = null;
			}
		}
	}

	/**
	 * Add an ID to this grid
	 * 
	 * @param id The id of the gizmo to be added.
	 * @param x The x coordinate to place the gizmo at.
	 * @param y The y coordinate to place the gizmo at.
	 */
	public void add(String id, int x, int y) {
		board[x][y] = id;
	}

	/**
	 * Sets the coordinates to null on the board.
	 * 
	 * @param x The x coordinate to delete a gizmo from.
	 * @param y The y coordinate to delete a gizmo from.
	 */
	public void delete(int x, int y) {
		board[x][y] = null;
	}

	/**
	 * Returns the String at the given
	 * coordinates
	 * 
	 * @param x The x coordinate to get an id from.
	 * @param y The y coordinate to get an id from.
	 * @return The string id of the gizmo found or null if the cell is empty.
	 */
	public String get(int x, int y) {
		return board[x][y];
	}

	/**
	 * Checks the coordinates passed in to see if it
	 * is empty or not
	 * 
	 * @param x The x coordinate to check.
	 * @param y The y coordinate to check.
	 * @return result True if the cell is not empty and false otherwise.
	 */
	public boolean check(int x, int y) {
		return get(x, y) != null;
	}

	/**
	 * Clears the grid
	 */
	public void clear() {
		// Fill it with nulls
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[i].length; j++) {
				board[i][j] = null;
			}
		}
	}

}
