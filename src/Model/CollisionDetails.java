package Model;

import physics.Vect;
import Model_Gizmos.Gizmo_I;

/**
 * Return the details of the collision we just had.
 * @author Barry
 *
 */
public  class CollisionDetails {
	/**Time until collision.*/
	private double tuc;
	/**The velocity of the ball?*/
	private Vect velo;
	/**The gizmo the ball will collide with.*/
	private Gizmo_I giz;

	/**
	 * Default constructor
	 * @param t
	 * 		The time until collision
	 * @param v
	 * 		The speed which the collision will result in
	 * @param currGiz
	 * 		The Gizmo we're colliding with
	 */
	public CollisionDetails(double t, Vect v, Gizmo_I currGiz) {
		tuc = t;
		velo = v;
		giz = currGiz;
	}

	/**
	 * @return The time until collision.
	 */
	public double getTuc() {
		return tuc;
	}

	/**
	 * @return The velocity of the ball.
	 */
	public Vect getVelo() {
		return velo;
	}

	/**
	 * @return The gizmo the ball will collide with.
	 */
	public Gizmo_I getGiz() {
		return giz;
	}

}
